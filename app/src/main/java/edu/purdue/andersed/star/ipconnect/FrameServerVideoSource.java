package edu.purdue.andersed.star.ipconnect;

import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.videosource.VideoSource;
import edu.purdue.andersed.streaming.ByteArrayMemoryPool;
import edu.purdue.andersed.streaming.ByteArrayWithLength;
import edu.purdue.andersed.streaming.DecodeListener;
import edu.purdue.andersed.streaming.Decoder;

public class FrameServerVideoSource extends VideoSource implements DecodeListener {

	// used by the mentor system, receives an incoming connection from a trainee system and supports receiving frames from the trainee client
	
	private static final String TAG = "FrameServerVideoSource";
	
	private Thread mFrameServerLoopThread;
	private boolean mRunning;
	private ServerSocket mServerSocket;


	private MainActivity mDelegate;
	
	Bitmap mReceivedBitmap = null;
	
	public FrameServerVideoSource(MainActivity delegate) {
		Log.d(TAG, "constructing FrameServer");
		
		mDelegate = delegate;
	}

	private void teardownIfNeeded() {
		Log.d(TAG, "teardownIfNeeded");
		mRunning = false;

		if (mServerSocket != null) {
			try {
				Log.d(TAG, "trying to close server socket");
				mServerSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			mServerSocket = null;
		}

		if (mFrameServerLoopThread != null && mFrameServerLoopThread.isAlive()) {
			Log.d(TAG, "interrupting thread");
			mFrameServerLoopThread.interrupt();
		}
	}

	@Override
	public void initAndStartVideoSource(SurfaceTexture surfaceTexture) {
		Log.d(TAG, "initializing FrameServer");

		teardownIfNeeded();

		if (Pref.getInstance().isEncodingDecodingFrames()) {
			int decodeWidth = ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor();
			int decodeHeight = ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor();
			Decoder.getInstance().init(this, decodeWidth, decodeHeight);	
		}

		mFrameServerLoopThread = new Thread(new FrameServerLoop());

		mRunning = true;

		mFrameServerLoopThread.start();
	}

	@Override
	public void stopVideoSource() {
		Log.d(TAG, "destroying FrameServer");
		teardownIfNeeded();
	}


	private class FrameServerLoop implements Runnable {
		private static final String TAG = "FrameServerLoop";

		public void run() {
			Log.d(TAG, "starting run()");

			try {

				int maxSizePerEncodedFrame = 65536*4;

				ByteArrayMemoryPool pool = new ByteArrayMemoryPool(4, maxSizePerEncodedFrame);

				byte[] lengthBytes = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder()).array();

				byte buff[] = new byte[maxSizePerEncodedFrame];

				Mat receivedMat = new Mat(ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor(), ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor(), PointOverlay.OPENCV_MAT_TYPE);
				Log.d(TAG, "set up mat for incoming frames, size is " + receivedMat.cols() + " x " + receivedMat.rows());

				while (mRunning && !Thread.currentThread().isInterrupted()) {
					try {

						int port = Pref.getInstance().getVideoServerSocketPort();
						Log.d(TAG, "starting server on port " + port);
						mServerSocket = new ServerSocket();
						mServerSocket.setReuseAddress(true);
						mServerSocket.bind(new InetSocketAddress(port));
						Log.d(TAG, "started server socket, waiting for connection...");

						while (mRunning && !Thread.currentThread().isInterrupted() && !mServerSocket.isClosed()) {

							Log.d(TAG, "waiting for connection...");
							Socket clientSocket = mServerSocket.accept();
							Log.d(TAG, "got client connection");

							InputStream inputStream = clientSocket.getInputStream();

							while (mRunning && clientSocket.isConnected()) {

								//Log.d(TAG, "reading in length bytes...");

								boolean lengthBytesHaveBeenRead = false;
								int numBytesReadSoFar = 0;
								while (!lengthBytesHaveBeenRead) {
									int numBytesRead = inputStream.read(lengthBytes, numBytesReadSoFar, lengthBytes.length - numBytesReadSoFar);
									//Log.d(TAG, "numBytesRead: " + numBytesRead);

									if (numBytesRead >= 0) {
										numBytesReadSoFar += numBytesRead;
									} else {
										lengthBytesHaveBeenRead = true;
									}

									//Log.d(TAG, "numBytesReadSoFar: " + numBytesReadSoFar);
									if (numBytesReadSoFar >= lengthBytes.length) {
										lengthBytesHaveBeenRead = true;
									}
								}
								//Log.d(TAG, "done reading all the length bytes");

								int numBytesInPayload = ByteBuffer.wrap(lengthBytes).order(ByteOrder.nativeOrder()).asIntBuffer().get();
								//Log.d(TAG, "numBytesInPayload: " + numBytesInPayload);

								int currentByteOffset = 0;
								int bytesReadInLastCall = 0;
								do {
									//Log.d(TAG, "reading...");
									bytesReadInLastCall = inputStream.read(buff, currentByteOffset, numBytesInPayload - currentByteOffset);
									if (bytesReadInLastCall >= 0) {
										currentByteOffset += bytesReadInLastCall;
									}
									//Log.d(TAG, "bytesReadInLastCall: " + bytesReadInLastCall);
									//Log.d(TAG, "currentByteOffset: " + currentByteOffset);
								}
								while (bytesReadInLastCall != -1 && currentByteOffset < numBytesInPayload);

								if (Pref.getInstance().isEncodingDecodingFrames()) {
									//Log.d(TAG, "got encoded bytes, size: " + currentByteOffset);

									if (currentByteOffset != numBytesInPayload) {
										break;
									} else {
										ByteArrayWithLength encodedBytes = pool.producerPoll();
										if (encodedBytes != null) {
											System.arraycopy(buff, 0, encodedBytes.getByteArray(), 0, currentByteOffset);
											encodedBytes.setLength(currentByteOffset);

											pool.handToConsumer(encodedBytes);

											Decoder.getInstance().setPool(pool);
										}
									}

								} else {
									//Log.d(TAG, "putting data into mat");
									receivedMat.put(0, 0, buff);

									onReceivedFrame(receivedMat);
								}
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
					int retryMillis = Pref.getInstance().getClientRetryMillis();
					Log.d(TAG, "frame server shut down. waiting for " + retryMillis + " ms to retry.");
					Thread.sleep(retryMillis);
				}
			} catch (InterruptedException e) {
				Log.d(TAG, "Interrupted: " + e.getMessage());
			} finally {
				Log.d(TAG, "closing server socket if needed");
				if (mServerSocket != null) {
					try {
						mServerSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					mServerSocket = null;
				}
			}
			Log.d(TAG, "ending run()");
		}
	}

	
	public void onReceivedFrame(Mat receivedMat) {
		if (mReceivedBitmap == null || mReceivedBitmap.getWidth() != receivedMat.width() || mReceivedBitmap.getHeight() != receivedMat.height()) {
			Log.d(TAG, "creating received bitmap");
			mReceivedBitmap = Bitmap.createBitmap(receivedMat.width(), receivedMat.height(), Bitmap.Config.ARGB_8888);	
		}

		try {
			Utils.matToBitmap(receivedMat, mReceivedBitmap);

			mDelegate.onNewFrameBitmapFromSocket(mReceivedBitmap);
		} catch (CvException e) {
			e.printStackTrace();
			Log.e(TAG, e.getMessage());
		}
	}

	@Override
	public void onReceivedDecodedFrame(Mat decodedRGBFrame) {
		onReceivedFrame(decodedRGBFrame);
	}
}
