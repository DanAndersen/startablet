package edu.purdue.andersed.star;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Debug;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Point;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ar.com.daidalos.afiledialog.FileChooserDialog;
import edu.purdue.andersed.star.annotations.AnimatedIncisionAnnotation;
import edu.purdue.andersed.star.annotations.AnimationAnnotation;
import edu.purdue.andersed.star.annotations.AnimationTemplate;
import edu.purdue.andersed.star.annotations.Annotation;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.AnnotationStateLogging;
import edu.purdue.andersed.star.annotations.AnnotationStates;
import edu.purdue.andersed.star.annotations.FullAnnotationSave;
import edu.purdue.andersed.star.annotations.MultiPointAnnotation;
import edu.purdue.andersed.star.annotations.PolygonAnnotation;
import edu.purdue.andersed.star.annotations.PolylineAnnotation;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.annotations.ToolType;
import edu.purdue.andersed.star.ipconnect.DataClient;
import edu.purdue.andersed.star.ipconnect.DataServer;
import edu.purdue.andersed.star.ipconnect.FrameClient;
import edu.purdue.andersed.star.ipconnect.FrameServerVideoSource;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.shaders.TexVideoShaderProgram;
import edu.purdue.andersed.star.shaders.Texture2DVideo;
import edu.purdue.andersed.star.videosource.CameraVideoSource;
import edu.purdue.andersed.star.videosource.FileVideoSource;
import edu.purdue.andersed.star.videosource.ScriptedSequenceVideoSource;
import edu.purdue.andersed.star.videosource.VideoSource;
import edu.purdue.andersed.star.wifidirect.NetworkUtil;
import edu.purdue.andersed.star.wifidirect.PeerConnection;
import edu.purdue.andersed.streaming.Decoder;
import edu.purdue.andersed.streaming.Encoder;

public class MainActivity extends Activity implements SurfaceTexture.OnFrameAvailableListener, OnTouchListener, OnClickListener, PreviewCallback {
    private static final String TAG = "MainActivity";

    private StarGLSurfaceView mGlSurfaceView;
    private STARRenderer mRenderer;
    private SurfaceTexture mSurfaceTexture;

    private RelativeLayout mUILayout;

    private ToggleButton mAddPointAnnotationButton;
    private ToggleButton mAddMultiPointAnnotationButton;
    private ToggleButton mAddLineAnnotationButton;
    private ToggleButton mAddPolygonAnnotationButton;
    private ToggleButton mAddAnimatedIncisionAnnotationButton;

    private List<ButtonToAddTool> mButtonsToAddTool;

    private Button mRequestStartSendingFramesButton;
    private Button mSendAnnotationsButton;

    private Button mSaveAnnotationsButton;
    private Button mLoadAnnotationsButton;

    private Button mMoveAllAnnotationsLeftButton;
    private Button mMoveAllAnnotationsRightButton;
    private Button mMoveAllAnnotationsUpButton;
    private Button mMoveAllAnnotationsDownButton;
    private Button mResetAnnotationTranslateButton;

    private Button mPrevVideoStageButton;
    private Button mNextVideoStageButton;
    private int mCurrentVideoStageIndex;
    private int mNumVideoStages;
    private TextView mStageTextView;
    private List<String> mVideoStageLabels;
    private List<String> mVideoDescriptionLabels;

    private UIMode mUIMode;

    private Button mClearGlobalAnnotationsButton;
    private Button mClearLocalAnnotationsButton;

    private Button mReplayLocalAnnotationsButton;

    private SeekBar mBrightnessOffsetSeekBar;
    private SeekBar mContrastOffsetSeekBar;
    private SeekBar mToolTransparencySeekBar;

    private Button mToggleLocalColorControlsButton;

    private ImageView mPreviewImageView;

    private VideoSource mVideoSource;

    private Integer mWorkingMultiPointAnnotationMemoryID;

    private Integer mWorkingPolylineAnnotationMemoryID;

    private Integer mWorkingPolygonAnnotationMemoryID;

    private Integer mWorkingToolAnnotationMemoryID;

    private Integer mWorkingAnimatedIncisionAnnotationMemoryID;

    private ToolType mActiveToolType;

    private Button mRemoveSelectedGlobalAnnotationButton;

    private TextView mEngineerTextView;
    private EngineerViewData mEngineerViewData;

    private DataServer mDataServer;
    private DataClient mDataClient;

    private boolean mShouldRenderNewFrames;

    private GridLayout mVideosGridLocalLayout;

    OrientationEventListener mOrientationEventListener;
    boolean mLocalAnnotationsContainerIsFlipped = false;

    View mLocalAnnotationsContainer;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            Log.d(TAG, "onManagerConnected");
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.d(TAG, "OpenCV loaded successfully");

                    onOpenCVLoaded();
                    break;
                }
                default:
                {
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    protected void onOpenCVLoaded() {
        if (isMentor()) {
            if (mDataServer != null) {
                mDataServer.destroy();
                mDataServer = null;
            }
            mDataServer = new DataServer();
            mDataServer.init();

        } else if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
            if (mDataClient != null) {
                mDataClient.destroy();
                mDataClient = null;
            }
            mDataClient = new DataClient();
            mDataClient.init();

            FrameClient.getInstance().init();
        }

        initializeDisplay();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        // Sets the default uncaught exception handler. This handler is invoked
        // in case any Thread dies due to an unhandled exception.
        Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler(
                "/mnt/sdcard/"));

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);




    }


    @Override
    protected void onStop() {
        super.onStop();

        Log.d(TAG, "onStop");
        if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
            Decoder.getInstance().destroy();
        }

        if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
            Encoder.getInstance().destroy();
        }

        if (mVideoSource != null) {
            mVideoSource.stopVideoSource();
        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        Toaster.initialize(this);

        Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);


        ScreenState.getInstance().initScreenSize(size.x, size.y);




        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean pref = prefs.getBoolean("checkbox_preference", false);
        Log.d(TAG, "pref = " + pref);


        setContentView(R.layout.main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        mShouldRenderNewFrames = true;

        Pref.getInstance().initialize(this);

        if (Pref.getInstance().isTracing()) {
            Debug.startMethodTracing();
        }

        try {
            if (Pref.getInstance().isLoggingEnabled()) {
                AnnotationStateLogging.getInstance().initLogging();
                FrameProcessorPerformanceLogging.getInstance().initLogging();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Log.d(TAG, "initializing OpenCV...");

        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    private void initializeDisplay() {
        Log.d(TAG, "initializeDisplay");




        mButtonsToAddTool = new ArrayList<ButtonToAddTool>();

        mEngineerViewData = new EngineerViewData();

        mGlSurfaceView = new StarGLSurfaceView(this);
        mRenderer = mGlSurfaceView.getRenderer();

        addContentView(mGlSurfaceView, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));

        Log.d(TAG, "setting up touch listener");
        mGlSurfaceView.setOnTouchListener(this);

        Log.d(TAG, "initializing video source");
        //mVideoSource = Config.USING_RECORDED_VIDEO ? new FileVideoSource(this) : new CameraVideoSource();

        if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
            // mentor

            mVideoSource = new FrameServerVideoSource(this);
        } else {
            // trainee
            if (Pref.getInstance().isUsingScriptedVideoSequence()) {
                mVideoSource = new ScriptedSequenceVideoSource(this);
            } else {
                mVideoSource = new CameraVideoSource(this);
            }

        }

        addOverlayView();

        mOrientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                if (orientation == ORIENTATION_UNKNOWN) return;

                if (mLocalAnnotationsContainerIsFlipped) {
                    if (orientation >= 315 || orientation <= 45) {
                        mLocalAnnotationsContainerIsFlipped = false;
                        syncLocalFutureStepsUI();
                    }
                } else {
                    if (orientation >= 135 && orientation <= 225) {
                        mLocalAnnotationsContainerIsFlipped = true;
                        syncLocalFutureStepsUI();
                    }
                }
            }
        };

        if (mOrientationEventListener.canDetectOrientation()){
            mOrientationEventListener.enable();
        }

    }

    public void startCamera(int textureID) {
        Log.d(TAG, "startCamera");

        Log.d(TAG, "setting up surface texture");
        mSurfaceTexture = new SurfaceTexture(textureID);
        mSurfaceTexture.setOnFrameAvailableListener(this);
        mRenderer.setSurfaceTexture(mSurfaceTexture);

        mVideoSource.initAndStartVideoSource(mSurfaceTexture);

        if (!Pref.getInstance().isUsingMockConnection()) {
            if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
                Log.d(TAG, "trainee should start sending frames by default to mentor");
                PeerConnection.getInstance().setShouldSendFramesToOtherUser(true);
            }
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();

        AnnotationStates.getInstance().onPause();

        mRenderer.getPointOverlay().onPause(this);

        if (mGlSurfaceView != null) {
            mGlSurfaceView.onPause();
        }

        if (Pref.getInstance().isLoggingEnabled()) {
            AnnotationStateLogging.getInstance().closeLog();
            FrameProcessorPerformanceLogging.getInstance().closeLog();
        }

        if (Pref.getInstance().isTracing()) {
            Debug.stopMethodTracing();
        }

        if (PeerConnection.getInstance().getOwnUserType().isMentor()) {
            if (mDataServer != null) {
                mDataServer.destroy();
                mDataServer = null;
            }
        } else if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {
            if (mDataClient != null) {
                mDataClient.destroy();
                mDataClient = null;
            }
            FrameClient.getInstance().destroy();
        }

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (mOrientationEventListener != null) {
            mOrientationEventListener.disable();
            mOrientationEventListener = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        //mGlSurfaceView.requestRender();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);

        float touchX = event.getX();
        float touchY = event.getY();

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                //Log.d(TAG, "Action was DOWN");

                if (mUIMode.equals(UIMode.RECEIVING_FRAMES)) {
                    updateMode(UIMode.EDIT_MODE);
                }

                if (mUIMode.equals(UIMode.EDIT_MODE)) {
                    onSelectedTool(null);
                    selectToolAnnotationAtScreenCoordinates(touchX, touchY);
                }
                else if (mUIMode.equals(UIMode.ADDING_POINTS)) {
                    createPointAtScreenCoordinates(touchX, touchY);
                } else if (mUIMode.equals(UIMode.ADDING_LINE)) {
                    createOrContinueLineAtScreenCoordinates(touchX, touchY);
                } else if (mUIMode.equals(UIMode.ADDING_ANIMATED_INCISION)) {
                    createOrContinueAnimatedIncisionAtScreenCoordinates(touchX, touchY);
                } else if (mUIMode.equals(UIMode.ADDING_POLYGON)) {
                    createOrContinuePolygonAtScreenCoordinates(touchX, touchY);
                } else if (mUIMode.equals(UIMode.ADDING_TOOL)) {
                    if (mActiveToolType != null) {
                        createToolAtScreenCoordinates(mActiveToolType, touchX, touchY);
                    } else {
                        Toaster.getInstance().toast("adding tool, but no active tool type selected");
                    }
                }

                return true;
            case (MotionEvent.ACTION_MOVE):
                //Log.d(TAG, "Action was MOVE");

                if (mUIMode.equals(UIMode.ADDING_TOOL) || mUIMode.equals(UIMode.EDIT_MODE) && mWorkingToolAnnotationMemoryID != null) {

                    int numPointers = event.getPointerCount();

                    if (numPointers > 1) {
                        // multitouch

                        float anchorTouchX = event.getX(0);
                        float anchorTouchY = event.getY(0);

                        float alignTouchX = event.getX(1);
                        float alignTouchY = event.getY(1);

                        updateActiveToolAnchorPointToScreenCoordinates(anchorTouchX, anchorTouchY);
                        updateActiveToolBasedOnMotionAtScreenCoordinates(alignTouchX, alignTouchY);
                    } else if (numPointers == 1) {

                        updateActiveToolAnchorPointToScreenCoordinates(touchX, touchY);

                        // single touch
                    }


                } else if (mUIMode.equals(UIMode.ADDING_LINE)) {
                    createOrContinueLineAtScreenCoordinates(touchX, touchY);
                } else if (mUIMode.equals(UIMode.ADDING_ANIMATED_INCISION)) {
                    createOrContinueAnimatedIncisionAtScreenCoordinates(touchX, touchY);
                } else if (mUIMode.equals(UIMode.ADDING_POLYGON)) {
                    createOrContinuePolygonAtScreenCoordinates(touchX, touchY);
                }

                return true;
            case (MotionEvent.ACTION_UP):
                //Log.d(TAG, "Action was UP");

                if (mWorkingPolygonAnnotationMemoryID != null) {
                    // this means we were working on a new polygon annotation that has just finished being drawn

                    if (isMentor()) {
                        NetworkUtil.sendCreatedAnnotation(this, mWorkingPolygonAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingPolygonAnnotationMemoryID));
                    }


                    mWorkingPolygonAnnotationMemoryID = null;
                }


                if (mWorkingPolylineAnnotationMemoryID != null) {
                    // this means we were working on a new polyline annotation that has just finished being drawn
                    if (isMentor()) {
                        NetworkUtil.sendCreatedAnnotation(this, mWorkingPolylineAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingPolylineAnnotationMemoryID));
                    }

                    mWorkingPolylineAnnotationMemoryID = null;
                }

                if (mWorkingAnimatedIncisionAnnotationMemoryID != null) {

                    // this means we were working on a new polyline annotation that has just finished being drawn
                    if (isMentor()) {
                        NetworkUtil.sendCreatedAnnotation(this, mWorkingAnimatedIncisionAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingAnimatedIncisionAnnotationMemoryID));
                    }

                    mWorkingAnimatedIncisionAnnotationMemoryID = null;
                }

                if (mUIMode.equals(UIMode.ADDING_TOOL) || mUIMode.equals(UIMode.EDIT_MODE) && mWorkingToolAnnotationMemoryID != null) {
                    if (isMentor()) {
                        NetworkUtil.sendUpdatedAnnotation(this, mWorkingToolAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingToolAnnotationMemoryID));
                    }

                }

                if (mUIMode.equals(UIMode.ADDING_TOOL)) {
                    onSelectedTool(null);
                }

                if (mUIMode.equals(UIMode.ADDING_MULTI_POINTS)) {
                    createOrContinueMultiPointAtScreenCoordinates(touchX, touchY);
                } else {
                    updateMode(UIMode.EDIT_MODE);
                }

                return true;
            case (MotionEvent.ACTION_CANCEL):
                Log.d(TAG, "Action was CANCEL");
                return true;
            case (MotionEvent.ACTION_OUTSIDE):
                Log.d(TAG, "Movement occurred outside bounds "
                        + "of current screen element");
                return true;
            default:
                return super.onTouchEvent(event);
        }


    }

    private void selectToolAnnotationAtScreenCoordinates(float touchX,
                                                         float touchY) {
        mRenderer.onRequestSelectionAt(new Point(touchX, touchY));
    }

    // returns the annotation state that this local device should be manipulating
    // if it's a mentor, then it should be manipulating the global state
    // if it's a trainee, then it should only be manipulating its own local (separate) state
    public AnnotationState getActiveAnnotationState() {
        AnnotationState activeAnnotationState = isMentor() ? AnnotationStates.getInstance().getGlobalState() : AnnotationStates.getInstance().getLocalState();
        return activeAnnotationState;
    }

    private void updateActiveToolAnchorPointToScreenCoordinates(float anchorTouchX, float anchorTouchY) {
        getActiveAnnotationState().updateToolAnchorPoint(mWorkingToolAnnotationMemoryID, new Point(anchorTouchX, anchorTouchY));
    }

    private void updateActiveToolBasedOnMotionAtScreenCoordinates(float touchX,
                                                                  float touchY) {
        if (mWorkingToolAnnotationMemoryID != null) {

            AnnotationMemory workingToolAnnotationMemory = getActiveAnnotationState().getAnnotations().get(mWorkingToolAnnotationMemoryID);

            if (workingToolAnnotationMemory != null) {
                Annotation annotation = workingToolAnnotationMemory.getAnnotation();
                if (annotation instanceof ToolAnnotation) {
                    ToolAnnotation toolAnnotation = (ToolAnnotation) annotation;
                    Point toolPoint = toolAnnotation.getPointInScreenSpace();

                    double dx = touchX - toolPoint.x;
                    double dy = toolPoint.y - touchY;

                    double radians = Math.atan2(dy, dx);

                    double degrees = STARUtils.radiansToDegrees(radians);

                    toolAnnotation.setDegrees(degrees);

                    double distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));

                    double scale = Math.max(2 * distance / ScreenState.getInstance().getScreenWidth(), Pref.getInstance().getMinimumToolScale());

                    toolAnnotation.setScale(scale);

                } else {
                    Log.e(TAG, "annotation is not a tool annotation");
                }
            } else {
                Log.e(TAG, "cannot update tool annotation when annotation memory is null");
            }
        } else {
            Log.e(TAG, "cannot update tool annotation when ID is null");
        }
    }

    private void createPointAtScreenCoordinates(float touchX, float touchY) {
        Log.d(TAG, "createToolAtScreenCoordinates");
        Integer createdPointAnnotationId = getActiveAnnotationState().addPointWithScreenCoordinates(this, touchX, touchY);

        if (isMentor()) {
            NetworkUtil.sendCreatedAnnotation(this, createdPointAnnotationId, getActiveAnnotationState().getAnnotations().get(createdPointAnnotationId));
        }

    }

    private void createToolAtScreenCoordinates(ToolType toolType, float touchX, float touchY) {
        Log.d(TAG, "createToolAtScreenCoordinates");
        Class<?> annotationClass = toolType.getToolTemplate().getAnnotationClass();

        if (annotationClass == ToolAnnotation.class) {
            mWorkingToolAnnotationMemoryID = getActiveAnnotationState().addToolWithScreenCoordinatesRotationAndScale(this, toolType, touchX, touchY, 0, Pref.getInstance().getDefaultToolScale());
        } else if (annotationClass == AnimationAnnotation.class) {
            mWorkingToolAnnotationMemoryID = getActiveAnnotationState().addAnimationWithScreenCoordinatesRotationAndScale(this, toolType, touchX, touchY, 0, Pref.getInstance().getDefaultToolScale(), mCurrentVideoStageIndex);
        } else {
            throw new IllegalArgumentException("unknown annotation class: " + annotationClass);
        }
        updateToolSelection(mWorkingToolAnnotationMemoryID, true);

        if (isMentor()) {
            NetworkUtil.sendCreatedAnnotation(this, mWorkingToolAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingToolAnnotationMemoryID));
        } else {
            syncLocalFutureStepsUI();
        }
    }

    private void createOrContinueMultiPointAtScreenCoordinates(float touchX, float touchY) {
        Log.d(TAG, "createOrContinueMultiPointAtScreenCoordinates");

        boolean newlyCreatedMultiPointAnnotation = false;

        if (mWorkingMultiPointAnnotationMemoryID == null) {
            newlyCreatedMultiPointAnnotation = true;
            mWorkingMultiPointAnnotationMemoryID = getActiveAnnotationState().initializeNewMultiPointAnnotation(this);
        }

        AnnotationMemory workingMultiPointAnnotationMemory = getActiveAnnotationState().getAnnotations().get(mWorkingMultiPointAnnotationMemoryID);
        if (workingMultiPointAnnotationMemory != null) {
            MultiPointAnnotation workingMultiPointAnnotation = (MultiPointAnnotation) workingMultiPointAnnotationMemory.getAnnotation();
            // TODO need to update the whole annotation with the current relevant keypoints/descriptors from the current frame
            workingMultiPointAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
            getActiveAnnotationState().updateAnnotationMemoryWithFrameData(workingMultiPointAnnotationMemory);
        }

        if (isMentor()) {
            if (newlyCreatedMultiPointAnnotation) {
                NetworkUtil.sendCreatedAnnotation(this, mWorkingMultiPointAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingMultiPointAnnotationMemoryID));
            } else {
                NetworkUtil.sendUpdatedAnnotation(this, mWorkingMultiPointAnnotationMemoryID, getActiveAnnotationState().getAnnotations().get(mWorkingMultiPointAnnotationMemoryID));
            }
        }

    }

    private void createOrContinuePolygonAtScreenCoordinates(float touchX, float touchY) {
        Log.d(TAG, "createOrContinuePolygonAtScreenCoordinates");
        if (mWorkingPolygonAnnotationMemoryID == null) {

            mWorkingPolygonAnnotationMemoryID = getActiveAnnotationState().initializeNewPolygonAnnotation(this);
        }

        AnnotationMemory workingPolygonAnnotationMemory = getActiveAnnotationState().getAnnotations().get(mWorkingPolygonAnnotationMemoryID);
        if (workingPolygonAnnotationMemory != null) {
            PolygonAnnotation workingPolygonAnnotation = (PolygonAnnotation) workingPolygonAnnotationMemory.getAnnotation();
            // TODO need to update the whole annotation with the current relevant keypoints/descriptors from the current frame
            workingPolygonAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
            getActiveAnnotationState().updateAnnotationMemoryWithFrameData(workingPolygonAnnotationMemory);
        }

    }

    private void createOrContinueAnimatedIncisionAtScreenCoordinates(float touchX, float touchY) {
        Log.d(TAG, "createOrContinueAnimatedIncisionAtScreenCoordinates");
        if (mWorkingAnimatedIncisionAnnotationMemoryID == null) {
            mWorkingAnimatedIncisionAnnotationMemoryID = getActiveAnnotationState().initializeNewAnimatedIncisionAnnotation(this);
        }

        AnnotationMemory workingAnimatedIncisionAnnotationMemory = getActiveAnnotationState().getAnnotations().get(mWorkingAnimatedIncisionAnnotationMemoryID);
        if (workingAnimatedIncisionAnnotationMemory != null) {
            AnimatedIncisionAnnotation workingAnimatedIncisionAnnotation = (AnimatedIncisionAnnotation) workingAnimatedIncisionAnnotationMemory.getAnnotation();
            workingAnimatedIncisionAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
            getActiveAnnotationState().updateAnnotationMemoryWithFrameData(workingAnimatedIncisionAnnotationMemory);
        }
    }

    private void createOrContinueLineAtScreenCoordinates(float touchX, float touchY) {
        Log.d(TAG, "createOrContinueLineAtScreenCoordinates");
        if (mWorkingPolylineAnnotationMemoryID == null) {

            mWorkingPolylineAnnotationMemoryID = getActiveAnnotationState().initializeNewPolylineAnnotation(this);
        }

        AnnotationMemory workingPolylineAnnotationMemory = getActiveAnnotationState().getAnnotations().get(mWorkingPolylineAnnotationMemoryID);
        if (workingPolylineAnnotationMemory != null) {
            PolylineAnnotation workingPolylineAnnotation = (PolylineAnnotation) workingPolylineAnnotationMemory.getAnnotation();
            workingPolylineAnnotation.addPointWithScreenCoordinates(new Point(touchX, touchY));
            getActiveAnnotationState().updateAnnotationMemoryWithFrameData(workingPolylineAnnotationMemory);
        }
    }

    private boolean isMentor() {
        return PeerConnection.getInstance().getOwnUserType().isMentor();
    }

    private void addOverlayView() {
        Log.d(TAG, "addOverlayView");

        if (isMentor() || Pref.getInstance().isUsingMockConnection()) {
            Log.d(TAG, "user is a mentor, give the mentor UI");
            LayoutInflater inflater = LayoutInflater.from(this);
            mUILayout = (RelativeLayout) inflater.inflate(R.layout.camera_overlay_mentor, null, false);

            mUILayout.setVisibility(View.VISIBLE);

            addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            mUILayout.bringToFront();

            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            mAddPointAnnotationButton = setupButtonToAddGeometry(R.id.add_point_annotation_button, UIMode.ADDING_POINTS);

            mAddMultiPointAnnotationButton = setupButtonToAddGeometry(R.id.add_multi_point_annotation_button, UIMode.ADDING_MULTI_POINTS);

            mAddLineAnnotationButton = setupButtonToAddGeometry(R.id.add_line_annotation_button, UIMode.ADDING_LINE);

            mAddPolygonAnnotationButton = setupButtonToAddGeometry(R.id.add_polygon_annotation_button, UIMode.ADDING_POLYGON);

            mAddAnimatedIncisionAnnotationButton = setupButtonToAddGeometry(R.id.add_animated_incision_annotation_button, UIMode.ADDING_ANIMATED_INCISION);




            //SurfaceView streamingSurfaceView = (SurfaceView) findViewById(R.id.streaming_surface_view);
            //StreamingVideoManager.getInstance().registerSurface(streamingSurfaceView);






            // --- tools ---

            GridLayout toolsGridLayout = (GridLayout) mUILayout.findViewById(R.id.tools_grid);

            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.BVM, R.string.bvm, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.ETTUBE, R.string.ettube, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.HEMOSTAT, R.string.hemostat, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.IODINESWAB, R.string.iodineswab, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.LONGHOOK, R.string.longhook, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.RETRACTOR, R.string.retractor, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SCALPEL, R.string.scalpel, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SCISSORS, R.string.scissors, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.STETHOSCOPE, R.string.stethoscope, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SURGICALTAPE, R.string.surgicaltape, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.SYRINGE, R.string.syringe, Constants.ICON_ROTATION_TOOL));
            mButtonsToAddTool.add(new ButtonToAddTool(this, toolsGridLayout, ToolType.TWEEZERS, R.string.tweezers, Constants.ICON_ROTATION_TOOL));

            // --- videos ---

            GridLayout videosGridLayout = (GridLayout) mUILayout.findViewById(R.id.videos_grid);

            mButtonsToAddTool.add(new ButtonToAddTool(this, videosGridLayout, ToolType.VIDEO_CRIC, R.string.blank, Constants.ICON_ROTATION_VIDEO));
            mButtonsToAddTool.add(new ButtonToAddTool(this, videosGridLayout, ToolType.VIDEO_CRIC_TRIMMED, R.string.blank, Constants.ICON_ROTATION_VIDEO));

            mPrevVideoStageButton = (Button) mUILayout.findViewById(R.id.prev_video_stage_button);
            mPrevVideoStageButton.setOnClickListener(this);
            mNextVideoStageButton = (Button) mUILayout.findViewById(R.id.next_video_stage_button);
            mNextVideoStageButton.setOnClickListener(this);
            mStageTextView = (TextView) mUILayout.findViewById(R.id.video_stage_textview);
            mCurrentVideoStageIndex = 0;
            mVideoStageLabels = ((AnimationTemplate) ToolType.VIDEO_CRIC_TRIMMED.getToolTemplate()).getVideoStageLabels();
            mVideoDescriptionLabels = ((AnimationTemplate) ToolType.VIDEO_CRIC_TRIMMED.getToolTemplate()).getVideoDescriptionLabels();
            mNumVideoStages = mVideoStageLabels.size();
            mStageTextView.setText(mVideoDescriptionLabels.get(mCurrentVideoStageIndex));


            // --- labels ---

            GridLayout labelsGridLayout = (GridLayout) mUILayout.findViewById(R.id.labels_grid);

            mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_CLOSE, R.string.blank, Constants.ICON_ROTATION_TEXT));
            mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_INCISION, R.string.blank, Constants.ICON_ROTATION_TEXT));
            mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_PALPATION, R.string.blank, Constants.ICON_ROTATION_TEXT));
            mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_REMOVE, R.string.blank, Constants.ICON_ROTATION_TEXT));
            mButtonsToAddTool.add(new ButtonToAddTool(this, labelsGridLayout, ToolType.TEXT_STITCH, R.string.blank, Constants.ICON_ROTATION_TEXT));

            // --- hands ---

            GridLayout handsGridLayout = (GridLayout) mUILayout.findViewById(R.id.hands_grid);

            mButtonsToAddTool.add(new ButtonToAddTool(this, handsGridLayout, ToolType.HAND_PALPATE, R.string.hand_palpate, Constants.ICON_ROTATION_HAND));
            mButtonsToAddTool.add(new ButtonToAddTool(this, handsGridLayout, ToolType.HAND_POINT, R.string.hand_point, Constants.ICON_ROTATION_HAND));
            mButtonsToAddTool.add(new ButtonToAddTool(this, handsGridLayout, ToolType.HAND_STRETCH, R.string.hand_stretch, Constants.ICON_ROTATION_HAND));


            mSaveAnnotationsButton = (Button) mUILayout.findViewById(R.id.save_annotations_button);
            mSaveAnnotationsButton.setOnClickListener(this);

            mLoadAnnotationsButton = (Button) mUILayout.findViewById(R.id.load_annotations_button);
            mLoadAnnotationsButton.setOnClickListener(this);

            mRemoveSelectedGlobalAnnotationButton = (Button) mUILayout.findViewById(R.id.remove_selected_global_annotation_button);
            if (mRemoveSelectedGlobalAnnotationButton != null) {
                mRemoveSelectedGlobalAnnotationButton.setOnClickListener(this);
            }

            mClearGlobalAnnotationsButton = (Button) mUILayout.findViewById(R.id.clear_global_annotations_button);
            if (mClearGlobalAnnotationsButton != null) {
                mClearGlobalAnnotationsButton.setOnClickListener(this);
            }

            mRequestStartSendingFramesButton = (Button) mUILayout.findViewById(R.id.request_start_sending_frames_button);
            mRequestStartSendingFramesButton.setOnClickListener(this);

            mSendAnnotationsButton = (Button) mUILayout.findViewById(R.id.send_annotation_state_button);
            mSendAnnotationsButton.setOnClickListener(this);

            mMoveAllAnnotationsLeftButton = (Button) mUILayout.findViewById(R.id.move_all_left_button);
            mMoveAllAnnotationsLeftButton.setOnClickListener(this);

            mMoveAllAnnotationsRightButton = (Button) mUILayout.findViewById(R.id.move_all_right_button);
            mMoveAllAnnotationsRightButton.setOnClickListener(this);

            mMoveAllAnnotationsUpButton = (Button) mUILayout.findViewById(R.id.move_all_up_button);
            mMoveAllAnnotationsUpButton.setOnClickListener(this);

            mMoveAllAnnotationsDownButton = (Button) mUILayout.findViewById(R.id.move_all_down_button);
            mMoveAllAnnotationsDownButton.setOnClickListener(this);

            mResetAnnotationTranslateButton = (Button) mUILayout.findViewById(R.id.reset_offset_button);
            mResetAnnotationTranslateButton.setOnClickListener(this);

            mPreviewImageView = (ImageView) mUILayout.findViewById(R.id.previewImageView);

            mEngineerTextView = (TextView) mUILayout.findViewById(R.id.engineer_textview);
            if (mEngineerTextView != null) {
                if (Pref.getInstance().isUsingEngineerView()) {
                    mEngineerTextView.setVisibility(View.VISIBLE);
                    mPreviewImageView.setVisibility(View.VISIBLE);
                } else {
                    mEngineerTextView.setVisibility(View.GONE);
                    mPreviewImageView.setVisibility(View.GONE);
                }
            }

            updateMode(UIMode.RECEIVING_FRAMES);
        } else {
            Log.d(TAG, "user is a trainee");

            LayoutInflater inflater = LayoutInflater.from(this);
            mUILayout = (RelativeLayout) inflater.inflate(R.layout.camera_overlay_trainee, null, false);

            mUILayout.setVisibility(View.VISIBLE);

            addContentView(mUILayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            View prevStepsContainers = findViewById(R.id.previous_steps_container);
            if (PeerConnection.getInstance().getOwnUserType().isTrainee() && Pref.getInstance().isShowingPreviousSteps()) {
                prevStepsContainers.setVisibility(View.VISIBLE);

                Button prevButton = (Button) findViewById(R.id.prev_step_button);
                Button nextButton = (Button) findViewById(R.id.next_step_button);
                ImageView stepImageView = (ImageView) findViewById(R.id.previous_step_imageview);

                PreviousStepsState.getInstance().registerUI(this, prevButton, nextButton, stepImageView);

            } else {
                prevStepsContainers.setVisibility(View.GONE);
            }





            if (Pref.getInstance().isUsingScriptedVideoSequence()) {
                Button advanceScriptedVideoButton = (Button) mUILayout.findViewById(R.id.advance_scripted_video_button);
                if (advanceScriptedVideoButton != null) {
                    advanceScriptedVideoButton.setVisibility(View.VISIBLE);
                    advanceScriptedVideoButton.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mVideoSource instanceof ScriptedSequenceVideoSource) {
                                ScriptedSequenceVideoSource ss = (ScriptedSequenceVideoSource) mVideoSource;
                                ss.advanceVideoStage();
                            }
                        }
                    });
                }
            }






            mLocalAnnotationsContainer = mUILayout.findViewById(R.id.local_annotations_container);

            // --- videos for trainee UI ---

            mVideosGridLocalLayout = (GridLayout) mUILayout.findViewById(R.id.videos_grid_local);

            mButtonsToAddTool.add(new ButtonToAddTool(this, mVideosGridLocalLayout, ToolType.VIDEO_CRIC, R.string.blank, Constants.ICON_ROTATION_VIDEO));
            mButtonsToAddTool.add(new ButtonToAddTool(this, mVideosGridLocalLayout, ToolType.VIDEO_CRIC_TRIMMED, R.string.blank, Constants.ICON_ROTATION_VIDEO));

            mPrevVideoStageButton = (Button) mUILayout.findViewById(R.id.prev_video_stage_button);
            mPrevVideoStageButton.setOnClickListener(this);
            mNextVideoStageButton = (Button) mUILayout.findViewById(R.id.next_video_stage_button);
            mNextVideoStageButton.setOnClickListener(this);
            mStageTextView = (TextView) mUILayout.findViewById(R.id.video_stage_textview);
            mCurrentVideoStageIndex = 0;
            mVideoStageLabels = ((AnimationTemplate) ToolType.VIDEO_CRIC_TRIMMED.getToolTemplate()).getVideoStageLabels();
            mVideoDescriptionLabels = ((AnimationTemplate) ToolType.VIDEO_CRIC_TRIMMED.getToolTemplate()).getVideoDescriptionLabels();
            mNumVideoStages = mVideoStageLabels.size();
            mStageTextView.setText(mVideoDescriptionLabels.get(mCurrentVideoStageIndex));

            mClearLocalAnnotationsButton = (Button) mUILayout.findViewById(R.id.clear_local_annotations_button);
            if (mClearLocalAnnotationsButton != null) {
                mClearLocalAnnotationsButton.setOnClickListener(this);
            }

            mReplayLocalAnnotationsButton = (Button) mUILayout.findViewById(R.id.replay_local_annotations_button);
            if (mReplayLocalAnnotationsButton != null) {
                mReplayLocalAnnotationsButton.setOnClickListener(this);
            }

            mBrightnessOffsetSeekBar = (SeekBar) mUILayout.findViewById(R.id.brightness_offset_seekbar);
            if (mBrightnessOffsetSeekBar != null) {
                mBrightnessOffsetSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        float newBrightnessOffset = (2.0f / 100.0f) * progress - 1.0f;

                        TexVideoShaderProgram.BrightnessOffset = newBrightnessOffset;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }

            mContrastOffsetSeekBar = (SeekBar) mUILayout.findViewById(R.id.contrast_offset_seekbar);
            if (mContrastOffsetSeekBar != null) {
                mContrastOffsetSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        float newContrastOffset = (2.0f / 100.0f) * progress - 1.0f;

                        TexVideoShaderProgram.ContrastOffset = newContrastOffset;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }

            mToggleLocalColorControlsButton = (Button) mUILayout.findViewById(R.id.toggle_local_color_controls_button);
            if (mToggleLocalColorControlsButton != null) {
                mToggleLocalColorControlsButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "clicked mToggleLocalColorControlsButton");
                        View localColorControls = mUILayout.findViewById(R.id.local_color_controls);
                        if (localColorControls != null) {
                            if (localColorControls.getVisibility() == View.VISIBLE) {
                                localColorControls.setVisibility(View.GONE);
                            } else {
                                localColorControls.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
            }

            mToolTransparencySeekBar = (SeekBar) mUILayout.findViewById(R.id.transparency_offset_seekbar);
            if (mToolTransparencySeekBar != null) {
                mToolTransparencySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        float newToolTransparency = (1.0f / 100.0f) * progress;

                        TexVideoShaderProgram.ToolTransparency = newToolTransparency;
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }

            syncLocalFutureStepsUI();




            mUILayout.bringToFront();

            mUILayout.setBackgroundColor(Color.TRANSPARENT);

            updateMode(UIMode.RECEIVING_FRAMES);
        }

        final TextView titlebarTextView = (TextView) this.findViewById(R.id.titlebar_text);
        if (titlebarTextView != null) {

            if (PeerConnection.getInstance().getOwnUserType() == UserType.MENTOR) {
                titlebarTextView.setText(R.string.titlebar_mentor);
                titlebarTextView.setTextColor(Color.RED);
            } else if (PeerConnection.getInstance().getOwnUserType() == UserType.TRAINEE) {
                titlebarTextView.setText(R.string.titlebar_blank);	// we don't want anything appearing on the trainee tablet to obstruct recorded images
                titlebarTextView.setTextColor(Color.GREEN);
            }

        }

    }

    private void syncLocalFutureStepsUI() {

        if (mLocalAnnotationsContainerIsFlipped) {
            mLocalAnnotationsContainer.setRotation(180);
        } else {
            mLocalAnnotationsContainer.setRotation(0);
        }

        int numLocalAnnotations = AnnotationStates.getInstance().getLocalState().getAnnotations().size();

        if (numLocalAnnotations > 0) {
            if (mVideosGridLocalLayout != null) {
                mVideosGridLocalLayout.setVisibility(View.GONE);
            }

            if (mClearLocalAnnotationsButton != null) {
                mClearLocalAnnotationsButton.setVisibility(View.VISIBLE);
            }

            if (mReplayLocalAnnotationsButton != null) {
                mReplayLocalAnnotationsButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (mVideosGridLocalLayout != null) {
                mVideosGridLocalLayout.setVisibility(View.VISIBLE);
            }

            if (mClearLocalAnnotationsButton != null) {
                mClearLocalAnnotationsButton.setVisibility(View.GONE);
            }

            if (mReplayLocalAnnotationsButton != null) {
                mReplayLocalAnnotationsButton.setVisibility(View.GONE);
            }
        }
    }

    private ToggleButton setupButtonToAddGeometry(int buttonResourceId, final UIMode uiMode) {
        ToggleButton toggleButton = (ToggleButton) mUILayout.findViewById(buttonResourceId);
        toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                onToggled(isChecked, uiMode);
            }
        });
        return toggleButton;
    }

    private void updateAllAnimationAnnotationsToIndex(int newVideoStageIndex) {
        Log.d(TAG, "updateAllAnimationAnnotationsToIndex: " + newVideoStageIndex);
        mStageTextView.setText(mVideoDescriptionLabels.get(mCurrentVideoStageIndex));

        Map<Integer, AnnotationMemory> annotations = getActiveAnnotationState().getAnnotations();

        if (annotations != null) {
            Iterator<Integer> iter = annotations.keySet().iterator();

            while (iter.hasNext()) {
                Integer id = iter.next();
                AnnotationMemory annotationMemory = annotations.get(id);

                if (id != null && annotationMemory != null) {
                    Annotation annotation = annotationMemory.getAnnotation();
                    if (annotation != null && annotation instanceof AnimationAnnotation) {
                        AnimationAnnotation animationAnnotation = (AnimationAnnotation) annotation;

                        animationAnnotation.updateVideoStage(newVideoStageIndex);

                        if (isMentor()) {
                            NetworkUtil.sendUpdatedAnnotation(this, id, annotationMemory);
                        }

                    }
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick");

        if (v == mClearGlobalAnnotationsButton) {
            Log.d(TAG, "pressed clear annotations button");

            updateMode(UIMode.EDIT_MODE);

            onSelectedTool(null);

            AnnotationStates.getInstance().getGlobalState().clearAnnotations(this, true);
        } if (v == mClearLocalAnnotationsButton) {
            Log.d(TAG, "pressed clear annotations button");

            updateMode(UIMode.EDIT_MODE);

            onSelectedTool(null);

            AnnotationStates.getInstance().getLocalState().clearAnnotations(this, false);

            syncLocalFutureStepsUI();

        } if (v == mReplayLocalAnnotationsButton) {
            Log.d(TAG, "pressed replay annotations button");

            updateMode(UIMode.EDIT_MODE);

            onSelectedTool(null);

            AnnotationStates.getInstance().getLocalState().replayAnimationAnnotations(this);

            syncLocalFutureStepsUI();

        } else if (v == mPrevVideoStageButton) {
            Log.d(TAG, "pressed mPrevVideoStageButton");
            mCurrentVideoStageIndex--;
            if (mCurrentVideoStageIndex < 0) {
                mCurrentVideoStageIndex = 0;
            }
            updateAllAnimationAnnotationsToIndex(mCurrentVideoStageIndex);

        } else if (v == mNextVideoStageButton) {
            Log.d(TAG, "pressed mNextVideoStageButton");
            mCurrentVideoStageIndex++;
            if (mCurrentVideoStageIndex >= mNumVideoStages) {
                mCurrentVideoStageIndex = mNumVideoStages - 1;
            }
            updateAllAnimationAnnotationsToIndex(mCurrentVideoStageIndex);

        } else if (v == mRequestStartSendingFramesButton) {
            Log.d(TAG, "pressed request start-sending-frames button");

            mShouldRenderNewFrames = true;

        } else if (v == mSendAnnotationsButton) {
            Log.d(TAG, "pressed send annotations button");

            NetworkUtil.sendAnnotations(this);

            mShouldRenderNewFrames = true;
            updateMode(UIMode.RECEIVING_FRAMES);
        } else if (v == mRemoveSelectedGlobalAnnotationButton) {
            Log.d(TAG, "pressed remove selected annotation button");

            if (mWorkingToolAnnotationMemoryID != null) {
                updateToolSelection(mWorkingToolAnnotationMemoryID, false);
                AnnotationStates.getInstance().getGlobalState().removeAnnotationWithID(this, mWorkingToolAnnotationMemoryID, true);
                mWorkingToolAnnotationMemoryID = null;
            }
        } else if (v == mSaveAnnotationsButton) {
            Log.d(TAG, "pressed save annotations button");

            Toaster.getInstance().toast("saving annotations...");

            FullAnnotationSave.saveAnnotationStateToFile();
        } else if (v == mLoadAnnotationsButton) {
            Log.d(TAG, "pressed load annotations button");


            // Create the dialog.
            FileChooserDialog dialog = new FileChooserDialog(MainActivity.this);

            // Assign listener for the select event.
            dialog.addListener(MainActivity.this.onFileSelectedListener);

            // Define start folder.
            //dialog.loadFolder(Environment.getExternalStorageDirectory() + "/");

            // Show the dialog.
            dialog.show();
        } else if (v == mMoveAllAnnotationsLeftButton) {

            Point p = new Point(-10, 0);

            Pref.getInstance().offsetGlobalAnnotationTranslate(p);
            getActiveAnnotationState().translateAllAnnotations(p);

        } else if (v == mMoveAllAnnotationsRightButton) {

            Point p = new Point(10, 0);

            Pref.getInstance().offsetGlobalAnnotationTranslate(p);
            getActiveAnnotationState().translateAllAnnotations(p);

        } else if (v == mMoveAllAnnotationsUpButton) {

            Point p = new Point(0,-10);

            Pref.getInstance().offsetGlobalAnnotationTranslate(p);
            getActiveAnnotationState().translateAllAnnotations(p);

        } else if (v == mMoveAllAnnotationsDownButton) {

            Point p = new Point(0,10);

            Pref.getInstance().offsetGlobalAnnotationTranslate(p);
            getActiveAnnotationState().translateAllAnnotations(p);

        } else if (v == mResetAnnotationTranslateButton) {

            Point negativeP = Pref.getInstance().getGlobalAnnotationTranslate();
            Point p = new Point(-negativeP.x, -negativeP.y);

            getActiveAnnotationState().translateAllAnnotations(p);

            Pref.getInstance().resetGlobalAnnotationTranslate();
        }
    }

    private FileChooserDialog.OnFileSelectedListener onFileSelectedListener = new FileChooserDialog.OnFileSelectedListener() {
        public void onFileSelected(Dialog source, File file) {

            boolean success = false;

            try {

                BufferedReader reader = new BufferedReader( new FileReader (file));
                String         line = null;
                StringBuilder  stringBuilder = new StringBuilder();
                String         ls = System.getProperty("line.separator");

                while( ( line = reader.readLine() ) != null ) {
                    stringBuilder.append( line );
                    stringBuilder.append( ls );
                }

                String annotationStateString = stringBuilder.toString();

                JSONObject annotationStateJson;

                annotationStateJson = new JSONObject(annotationStateString);

                getActiveAnnotationState().updateFromJSONObject(annotationStateJson);
                getActiveAnnotationState().translateAllAnnotations(Pref.getInstance().getGlobalAnnotationTranslate());

                success = true;
            } catch (Exception e) {
                e.printStackTrace();

                success = false;

                Toaster.getInstance().toast("error: " + e.getMessage());
            }

            if (success) {
                source.hide();
                Toaster.getInstance().toast("loaded annotations from file " + file.getAbsolutePath());
            }
        }
        public void onFileSelected(Dialog source, File folder, String name) {
            source.hide();
            Toast toast = Toast.makeText(MainActivity.this, "File created: " + folder.getName() + "/" + name, Toast.LENGTH_LONG);
            toast.show();
        }
    };

    protected void onToolToggled(boolean isChecked, ToolType toolType) {
        if (isChecked) {
            mActiveToolType = toolType;
            updateMode(UIMode.ADDING_TOOL);
        } else if (mActiveToolType == toolType) {
            updateMode(UIMode.EDIT_MODE);
        }
    }

    void onToggled(boolean isChecked, UIMode uiMode) {
        if (isChecked) {
            updateMode(uiMode);
        } else if (mUIMode == uiMode) {
            updateMode(UIMode.EDIT_MODE);
        }
    }

    private void updateMode(UIMode uiMode) {
        mUIMode = uiMode;
        updateAllModes();
    }

    private void updateAllModes() {

        /*
        if (mUIMode != UIMode.RECEIVING_FRAMES) {
            mShouldRenderNewFrames = false;
        }
        */
        mShouldRenderNewFrames = true;

        if (mUIMode != UIMode.ADDING_MULTI_POINTS) {
            mWorkingMultiPointAnnotationMemoryID = null;
        }

        if (mUIMode != UIMode.ADDING_POLYGON) {
            mWorkingPolygonAnnotationMemoryID = null;
        }

        if (mUIMode != UIMode.ADDING_LINE) {
            mWorkingPolylineAnnotationMemoryID = null;
        }

        if (mUIMode != UIMode.ADDING_ANIMATED_INCISION) {
            mWorkingAnimatedIncisionAnnotationMemoryID = null;
        }

        if (mUIMode != UIMode.ADDING_TOOL) {
            mActiveToolType = null;
        }

        if (mAddPolygonAnnotationButton != null) {
            mAddPolygonAnnotationButton.setChecked(mUIMode == UIMode.ADDING_POLYGON);
        }

        if (mAddPointAnnotationButton != null) {
            mAddPointAnnotationButton.setChecked(mUIMode == UIMode.ADDING_POINTS);
        }

        if (mAddMultiPointAnnotationButton != null) {
            mAddMultiPointAnnotationButton.setChecked(mUIMode == UIMode.ADDING_MULTI_POINTS);
        }

        if (mAddLineAnnotationButton != null) {
            mAddLineAnnotationButton.setChecked(mUIMode == UIMode.ADDING_LINE);
        }

        if (mAddAnimatedIncisionAnnotationButton != null) {
            mAddAnimatedIncisionAnnotationButton.setChecked(mUIMode == UIMode.ADDING_ANIMATED_INCISION);
        }

        if (mButtonsToAddTool != null) {
            for (ButtonToAddTool buttonToAddTool : mButtonsToAddTool) {
                buttonToAddTool.setCheckedState();
            }
        }

    }

    public void updatePreviewImage(Bitmap bitmap) {
        if (mPreviewImageView != null) {
            mPreviewImageView.setImageBitmap(bitmap);
        } else {
            Log.e(TAG, "preview imageview is null");
        }
    }

    public void onNewFrameBitmapFromSocket(Bitmap bitmap) {
        //Log.d(TAG, "onNewFrameBitmapFromSocket");
        if (mShouldRenderNewFrames) {
            // TODO Auto-generated method stub
            if (mRenderer.getCameraViewBackground() instanceof Texture2DVideo) {
                Texture2DVideo texture2DVideo = (Texture2DVideo) mRenderer.getCameraViewBackground();

                // setting the frame bitmap for our texture2dvideo
                texture2DVideo.setFrameBitmap(bitmap);
            } else {
                Log.e(TAG, "this cameraviewbackground can't do anything with this bitmap frame");
            }

            this.mRenderer.onNewFrameBitmapFromSocket(bitmap);
        }
    }

    private void updateToolSelection(Integer toolAnnotationMemoryID, boolean selected) {
        if (toolAnnotationMemoryID != null) {
            AnnotationMemory selectedAnnotation = getActiveAnnotationState().getAnnotations().get(toolAnnotationMemoryID);
            if (selectedAnnotation != null) {
                Annotation annotation = selectedAnnotation.getAnnotation();
                if (annotation != null && annotation instanceof ToolAnnotation) {
                    ((ToolAnnotation) annotation).setSelected(selected);
                }
            }
        }
    }

    public void onSelectedTool(Integer toolID) {
        if (mWorkingToolAnnotationMemoryID != toolID) {
            updateToolSelection(mWorkingToolAnnotationMemoryID, false);
            mWorkingToolAnnotationMemoryID = toolID;
            updateToolSelection(mWorkingToolAnnotationMemoryID, true);
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRemoveSelectedGlobalAnnotationButton != null) {
                    mRemoveSelectedGlobalAnnotationButton.setEnabled(mWorkingToolAnnotationMemoryID != null);
                }
            }
        });
    }

    public UIMode getUIMode() {
        return mUIMode;
    }

    public ToolType getActiveToolType() {
        return mActiveToolType;
    }

    private void updateEngineerTextView() {
        if (mEngineerTextView != null && mEngineerViewData != null) {
            mEngineerTextView.setText(mEngineerViewData.getText());
        }
    }

    public void onFPSUpdated(long fps, long elapsedMillis) {
        if (mEngineerViewData != null) {
            mEngineerViewData.setFPS((int) fps);
            mEngineerViewData.setMillisSinceLastFrame((int) elapsedMillis);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateEngineerTextView();
                }
            });
        }
    }

    public void onNumKeyPointsUpdated(int numKeyPoints) {
        if (mEngineerViewData != null) {
            mEngineerViewData.setNumKeyPoints(numKeyPoints);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateEngineerTextView();
                }
            });
        }
    }

    public STARRenderer getRenderer() {
        return mRenderer;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (mShouldRenderNewFrames) {
            if (mRenderer.getPointOverlay() != null) {
                mRenderer.getPointOverlay().onPreviewFrame(data, camera);
            }
        }
        camera.addCallbackBuffer(data);
    }

    public DataServer getDataServer() {
        return mDataServer;
    }

    public void updateVideoSourceTexImage() {
        if (PeerConnection.getInstance().getOwnUserType().isTrainee()) {

            if (mVideoSource instanceof ScriptedSequenceVideoSource) {

                // need to make sure everything is properly inited and on the right video stage
                ScriptedSequenceVideoSource ss = (ScriptedSequenceVideoSource) mVideoSource;

                if (!ss.isMediaPlayerInitialized()) {
                    Log.d(TAG, "media player is not initialized for ScriptedSequenceVideoSource, starting it");
                    ss.startMediaPlayer(this);
                }

                // now that the mediaplayer is (hopefully) initialized, update the tex image
                if (ss.isMediaPlayerInitialized()) {
                    if (ss.isActive()) {

                        if (!ss.isPlayingCorrectVideo()) {
                            Log.e(TAG, "caught the ScriptedSequenceVideoSource playing the wrong video clip, restarting the media player");
                            ss.startMediaPlayer(this);
                        }

                        SurfaceTexture st = mSurfaceTexture;
                        if (st != null) {
                            try {
                                st.updateTexImage();
                            } catch (IllegalStateException e) {
                                Log.e(TAG, e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            Log.e(TAG, "not updating tex image in surface texture, because surfacetexture is null");
                        }
                    } else {
                        Log.e(TAG, "not updating tex image because ScriptedSequenceVideoSource is not marked as active");
                    }
                } else {
                    Log.e(TAG, "not updating tex image because ScriptedSequenceVideoSource does not have media player initialized");
                }

            } else {
                // just a normal camera video source

                //Log.d(TAG, "the trainee is using a camera source that is directly updating a texture for us");
                mSurfaceTexture.updateTexImage(); // It will implicitly bind its texture to the GL_TEXTURE_EXTERNAL_OES texture target.
            }



        }
    }
}
