package edu.purdue.andersed.star.videosource;

public class FrameErrorData {
	
	private int mFrameNumber;
	private int mFrameInRecordingNumber;
	private double mError;

	public FrameErrorData(int frameNumber, int frameInRecordingNumber, double error) {
		mFrameNumber = frameNumber;
		mFrameInRecordingNumber = frameInRecordingNumber;
		mError = error;
	}
	
	public int getFrameNumber() {
		return mFrameNumber;
	}

	public int getFrameInRecordingNumber() {
		return mFrameInRecordingNumber;
	}

	public double getError() {
		return mError;
	}

}
