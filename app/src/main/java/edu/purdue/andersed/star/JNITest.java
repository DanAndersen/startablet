package edu.purdue.andersed.star;

/**
 * Created by andersed on 9/14/2016.
 */

import android.util.Log;

public class JNITest {

    static {
        try
        {
            Log.d("JNITest", "loading hello jni");
            System.loadLibrary("hello-android-jni");
            Log.d("JNITest", "loaded hello jni");
        }
        catch( UnsatisfiedLinkError e )
        {
            System.err.println("Native code library failed to load.\n" + e);
            throw new RuntimeException("unable to load JNI module.");
        }
    }

    public native String getMsgFromJni();
}
