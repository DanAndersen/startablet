package edu.purdue.andersed.star.videosource;

import android.graphics.SurfaceTexture;

public abstract class VideoSource {
	
	private static final String TAG = "VideoSource";	
		
	public abstract void initAndStartVideoSource(SurfaceTexture surfaceTexture);
	
	public abstract void stopVideoSource();
}
