package edu.purdue.andersed.star.shaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES30;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;

import org.opencv.core.Point;

import java.nio.IntBuffer;
import java.util.EnumMap;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.annotations.AnimationAnnotation;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.annotations.ToolTemplate;
import edu.purdue.andersed.star.annotations.ToolType;
import edu.purdue.andersed.star.geometry.Quad;
import edu.purdue.andersed.star.settings.Pref;

public class TexToolShaderProgram {

    private static final String TAG = "TexToolShaderProgram";

    private static final String VERTEX_SHADER =
            "attribute vec4 vertexPosition; \n" +
                    "uniform mat4 uMVPMatrix; \n" +
                    "attribute vec2 vertexTexCoord; \n" +
                    "varying vec2 texCoord; \n" +
                    "void main() \n" +
                    "{ \n" +
                    "    gl_Position = uMVPMatrix * vertexPosition; \n" +

                    "    texCoord = vertexTexCoord; \n" +
                    "} \n"
            ;

    private static final String FRAGMENT_SHADER =
            "precision mediump float; \n" +
            "varying vec2 texCoord; \n" +
            "uniform sampler2D texSampler; \n" +
            "uniform float toolTransparency; \n" +
            "uniform bool isSelected; \n" +
            "uniform bool drawingSelectableLayer; \n" +
            "uniform vec4 toolColor; \n" +
            "void main() \n" +
            "{ \n" +
            "    vec4 textureColor = texture2D(texSampler, texCoord); \n" +
            "    if (drawingSelectableLayer) { \n" +
            "        if (textureColor.a <= 0.0) { \n" +
            "            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0); \n" +
            "        }  else { \n" +
            "            gl_FragColor = toolColor; \n" +
            "        } \n" +
            "    } else { \n" +
            "        if (isSelected) { " +
            "            textureColor.rgb = mix(textureColor.rgb, vec3(1.0, 0.0, 0.0), 0.5); " +
            "        } " +
            "        gl_FragColor = vec4(textureColor.rgb, textureColor.a - toolTransparency); \n" +
            "    } \n" +
            "} \n"
    ;

    private int mShaderProgramID;

    private int mVertexHandle;

    private int mMVPMatrixHandle;

    private int mTextureCoordHandle;
    private int mTexSamplerHandle;
    private int mToolTransparencyHandle;
    private int mIsSelectedHandle;
    private int mDrawingSelectableLayerHandle;
    private int mToolColorHandle;

    public TexToolShaderProgram(Context context) {
        setupShaderProgram(context);
    }

    private void setupShaderProgram(Context context) {
        Log.d(TAG, "creating shader program");

        Log.d(TAG, "creating geometry");

        mShaderProgramID = STARUtils.createProgramFromShaderSrc(VERTEX_SHADER, FRAGMENT_SHADER);
        STARUtils.checkGLError("createProgramFromShaderSrc");

        Log.d(TAG, "setting up handles");
        mVertexHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexPosition");
        STARUtils.checkGLError("glGetAttribLocation");

        mMVPMatrixHandle = GLES30.glGetUniformLocation(mShaderProgramID, "uMVPMatrix");
        STARUtils.checkGLError("glGetUniformLocation");
        mToolTransparencyHandle = GLES30.glGetUniformLocation(mShaderProgramID, "toolTransparency");
        STARUtils.checkGLError("glGetUniformLocation");

        mIsSelectedHandle = GLES30.glGetUniformLocation(mShaderProgramID, "isSelected");
        STARUtils.checkGLError("glGetUniformLocation");

        mDrawingSelectableLayerHandle = GLES30.glGetUniformLocation(mShaderProgramID, "drawingSelectableLayer");
        STARUtils.checkGLError("glGetUniformLocation");

        mToolColorHandle = GLES30.glGetUniformLocation(mShaderProgramID, "toolColor");
        STARUtils.checkGLError("glGetUniformLocation");

        mTextureCoordHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexTexCoord");
        STARUtils.checkGLError("glGetAttribLocation");
        mTexSamplerHandle = GLES30.glGetAttribLocation(mShaderProgramID, "texSampler");
        STARUtils.checkGLError("glGetAttribLocation");

        setupToolTemplateData(context);
    }

    public void setupToolTemplateData(Context context) {
        Log.d(TAG, "setting up the geometries and textures for each tool type");
        for (ToolType tt : ToolType.values()) {
            ToolTextureStorage.getInstance().setupTextureAndGeometryForToolTemplate(context, tt);
        }
    }

    public void teardownToolTemplateData(Context context) {
        Log.d(TAG, "tearing down the geometries and textures for each tool type");
        for (ToolType tt : ToolType.values()) {
            ToolTextureStorage.getInstance().teardownTextureAndGeometryForToolTemplate(context, tt);
        }
    }

    public void drawTool(Context context, ToolAnnotation toolAnnotation, boolean isSelected, boolean drawingSelectableLayer) {

        ToolType toolType = toolAnnotation.getToolType();

        Point anchorPoint = toolAnnotation.getPointInScreenSpace();

        if (anchorPoint != null) {


            Quad geometry = ToolTextureStorage.getInstance().getGeometry(toolType);

            if (geometry != null) {

                GLES30.glUseProgram(mShaderProgramID);
                STARUtils.checkGLError("glUseProgram");

                GLES30.glVertexAttribPointer(mVertexHandle, 3, GLES30.GL_FLOAT, false, 0, geometry.getVertices());
                STARUtils.checkGLError("glVertexAttribPointer");
                GLES30.glVertexAttribPointer(mTextureCoordHandle, 2, GLES30.GL_FLOAT, false, 0, geometry.getTexCoords());
                STARUtils.checkGLError("glVertexAttribPointer");
                GLES30.glEnableVertexAttribArray(mVertexHandle);
                STARUtils.checkGLError("glEnableVertexAttribArray");
                GLES30.glEnableVertexAttribArray(mTextureCoordHandle);
                STARUtils.checkGLError("glEnableVertexAttribArray");

                int spriteTextureID = ToolTextureStorage.getInstance().getSpriteTextureID(toolType);

                GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
                STARUtils.checkGLError("glActiveTexture");
                GLES30.glBindTexture(GLES30.GL_TEXTURE_2D,
                        spriteTextureID);
                STARUtils.checkGLError("glBindTexture");
                GLES30.glUniform1i(mTexSamplerHandle, 0);
                STARUtils.checkGLError("glUniform1i");






                GLES30.glUniform1f(mToolTransparencyHandle, Pref.getInstance().getToolTransparency());
                STARUtils.checkGLError("glUniform1f");

                GLES30.glUniform1i(mDrawingSelectableLayerHandle, drawingSelectableLayer ? 1 : 0);
                STARUtils.checkGLError("glUniform1i");

                GLES30.glUniform1i(mIsSelectedHandle, isSelected ? 1 : 0);
                STARUtils.checkGLError("glUniform1i");

                float[] mvpMatrix = new float[16];

                double degrees = toolAnnotation.getDegrees();
                double scale = toolAnnotation.getScale();
                int color = toolAnnotation.getSelectableColor();

                //Log.d(TAG, "degrees: " + degrees);
                //Log.d(TAG, "scale: " + scale);

                Matrix.setIdentityM(mvpMatrix, 0);

                Matrix.translateM(mvpMatrix, 0,
                        (float)(2 * (anchorPoint.x - ScreenState.getInstance().getHalfScreenWidth()) / (float) ScreenState.getInstance().getScreenWidth()),
                        (float)(-2 * (anchorPoint.y - ScreenState.getInstance().getHalfScreenHeight()) / (float) ScreenState.getInstance().getScreenHeight()),
                        0.0f);

                Matrix.scaleM(mvpMatrix, 0, 1.0f, ScreenState.getInstance().getAspectRatio(), 1.0f);

                Matrix.scaleM(mvpMatrix, 0, (float) scale, (float) scale, 1.0f);

                Matrix.rotateM(mvpMatrix, 0, (float) degrees, 0.0f, 0.0f, 1.0f);

                Matrix.translateM(mvpMatrix, 0, geometry.getScaledOffsetFromCenterX(), geometry.getScaledOffsetFromCenterY(), 0.0f);

                float r = (float) Color.red(color) / 255;
                float g = (float) Color.green(color) / 255;
                float b = (float) Color.blue(color) / 255;

                //Log.d(TAG, "color: " + r + " " + g + " " + b);

                GLES30.glUniform4f(mToolColorHandle, r, g, b, 255);
                STARUtils.checkGLError("glUniform4f");

                GLES30.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
                STARUtils.checkGLError("glUniformMatrix4fv");

                GLES30.glDrawElements(GLES30.GL_TRIANGLES,
                        geometry.getNumObjectIndex(), GLES30.GL_UNSIGNED_SHORT,
                        geometry.getIndices());
                STARUtils.checkGLError("glDrawElements");
                GLES30.glDisableVertexAttribArray(mVertexHandle);
                STARUtils.checkGLError("glDisableVertexAttribArray");
                GLES30.glDisableVertexAttribArray(mTextureCoordHandle);
                STARUtils.checkGLError("glDisableVertexAttribArray");

            } else {
                Log.e(TAG, "tried to draw tex video but quad geometry was null");
            }

        } else {
            Log.e(TAG, "anchor point for drawing tool was null");
        }



    }
}
