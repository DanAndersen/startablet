package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class PolygonAnnotation extends Annotation {

	private static final String TAG = "PolygonAnnotation";


	
	public PolygonAnnotation(MatOfPoint2f pointMat) {
		mPointData.setPointsMat(pointMat);
	}

	public PolygonAnnotation() {
		Log.d(TAG, "creating empty PolygonAnnotation");
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_POLYGON;
	}
	
	@Override
	String getGeometryString() {
		return getPointsInScreenSpace().toString();
	}

    @Override
    public PointData getPointData() {
        return mPointData;
    }

    @Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
        pointOverlay.drawPointsWithColor(mPointData, color);
        pointOverlay.drawLinesWithColorAndThickness(mPointData, color, thickness, true, -1);
	}
	
	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		drawWithColorAndThickness(pointOverlay, color, Pref.getInstance().getDefaultLineThickness());
	}
	
	@Override
	public JSONObject toJSON() throws JSONException {
		
		JSONObject polygonAnnotationObject = new JSONObject();
		
		polygonAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		polygonAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(mPointData.getPointsMat()));
		
		return polygonAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		PolygonAnnotation polygonAnnotation = new PolygonAnnotation(pointMat);
		
		return polygonAnnotation;
	}



	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}
}
