package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class AnnotationMemory {
	// contains the current coordinates of an annotation, as well as info about the image around it when it was first created
	
	// the goal is to have individual homographies for each annotation, where the homography is from t=0 to t=n, instead of t=n-1 to t=n

	private static final String TAG = "AnnotationMemory";
	
	private Annotation mAnnotation;	// the coords of the annotation as it was when first created (or last updated)

	private static final String TAG_ANNOTATION = "annotation";

	private boolean mIsValid;
	
	public AnnotationMemory(Annotation initialAnnotation) {
		initializeMemory(initialAnnotation);
	}

	public void initializeMemory(Annotation annotation) {
		
		mIsValid = true;
		
		mAnnotation = annotation;
	}

	public Annotation getAnnotation() {
		return mAnnotation;
	}

	public JSONObject toSimpleJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject annotationMemoryObject = new JSONObject();
		
		annotationMemoryObject.put(TAG_ANNOTATION, mAnnotation.toJSON());
		
		//Log.d(TAG, "result: " + annotationMemoryObject.toString());
		return annotationMemoryObject;
	}
	
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject annotationMemoryObject = new JSONObject();
		
		annotationMemoryObject.put(TAG_ANNOTATION, mAnnotation.toJSON());

		//Log.d(TAG, "result: " + annotationMemoryObject.toString());
		return annotationMemoryObject;
	}

	public static AnnotationMemory fromJSON(JSONObject annotationMemoryObject) throws JSONException {
		
		Annotation annotation = getAnnotationFromJSON(annotationMemoryObject.getJSONObject(TAG_ANNOTATION));
		AnnotationMemory annotationMemory = new AnnotationMemory(annotation);

		return annotationMemory;
	}


	private static Annotation getAnnotationFromJSON(JSONObject annotationObject) throws JSONException {
		Log.d(TAG, "fromJSON()");
		String annotationType = annotationObject.getString(Annotation.TAG_ANNOTATION_TYPE);
		
		if (Annotation.TYPE_POINT.equals(annotationType)) {
			return PointAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_POLYLINE.equals(annotationType)) {
			return PolylineAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_POLYGON.equals(annotationType)) {
			return PolygonAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_TOOL.equals(annotationType)) {
			return ToolAnnotation.fromJSON(annotationObject);
		}  else if (Annotation.TYPE_ANIMATION.equals(annotationType)) {
			return AnimationAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_MULTI_POINT.equals(annotationType)) {
			return MultiPointAnnotation.fromJSON(annotationObject);
		} else if (Annotation.TYPE_ANIMATED_INCISION.equals(annotationType)) {
			return AnimatedIncisionAnnotation.fromJSON(annotationObject);
		} else {
			throw new IllegalStateException("unknown annotation type: " + annotationType);
		}
	}

	public boolean isValid() {
		return mIsValid;
	}


}
