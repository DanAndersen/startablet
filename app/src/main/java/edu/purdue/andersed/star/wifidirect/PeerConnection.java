package edu.purdue.andersed.star.wifidirect;

import android.util.Log;

import org.opencv.core.Mat;

import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.UserType;
import edu.purdue.andersed.star.ipconnect.FrameClient;

public class PeerConnection {

	private static PeerConnection instance;
	
	private static final String TAG = "PeerConnection";
		
	private UserType mOwnUserType;
	
	private String mMentorIP;
		
	private boolean mShouldSendFramesToOtherUser = false;
	
	private PeerConnection() {
		Log.d(TAG, "initializing PeerConnection");
	}
	
	public static PeerConnection getInstance() {
		if (instance == null) {
			instance = new PeerConnection();
		}
		return instance;
	}
	
	public UserType getOwnUserType() {
		if (mOwnUserType == null) {
			throw new IllegalStateException("mOwnUserType not set yet");
		}
		
		return mOwnUserType;
	}
	
	public void setOwnUserType(UserType userType) {
		mOwnUserType = userType;
	}
	
	public void setShouldSendFramesToOtherUser(boolean value) {
		mShouldSendFramesToOtherUser = value;
	}
	
	public boolean shouldSendFramesToOtherUser() {
		return mShouldSendFramesToOtherUser;
	}
	
	public void sendFrameDataToOtherUser(Mat mat) {
		FrameClient.getInstance().requestSendFrame(mat);
	}

	public void setMentorIP(String mentorIP) {
		mMentorIP = mentorIP;
	}

	public String getMentorIP() {
		return mMentorIP;
	}
}
