package edu.purdue.andersed.star.wifidirect;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.AnnotationStates;
import edu.purdue.andersed.star.commands.CreateAnnotationCommand;
import edu.purdue.andersed.star.commands.DeleteAnnotationCommand;
import edu.purdue.andersed.star.commands.SendAnnotationsCommand;
import edu.purdue.andersed.star.commands.UpdateAnnotationCommand;
import edu.purdue.andersed.star.ipconnect.DataServer;

public class NetworkUtil {

	private static final String TAG = "NetworkUtil";
	
	public static void sendDeleteAnnotationCommand(MainActivity delegate, Integer annotationId) {
		Log.d(TAG, "sendDeleteAnnotationCommand()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {
			DeleteAnnotationCommand deleteAnnotationCommand = new DeleteAnnotationCommand(annotationId);
			
			dataServer.requestSendCommand(deleteAnnotationCommand);
		} else {
			Log.e(TAG, "ERROR: tried to send delete annotation, but dataServer was null");
		}
	}
	
	public static void sendCreatedAnnotation(MainActivity delegate, Integer newAnnotationId, AnnotationMemory newAnnotationMemory) {
		Log.d(TAG, "sendCreatedAnnotation()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {
			try {
				CreateAnnotationCommand createAnnotationCommand = new CreateAnnotationCommand(newAnnotationId, newAnnotationMemory.toJSON());
				
				dataServer.requestSendCommand(createAnnotationCommand);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Log.e(TAG, "ERROR: tried to send created annotation, but dataServer was null");
		}
	}
	
	public static void sendUpdatedAnnotation(MainActivity delegate, Integer updatedAnnotationId,
			AnnotationMemory updatedAnnotationMemory) {
		Log.d(TAG, "sendUpdatedAnnotation()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {

			try {
				UpdateAnnotationCommand updateAnnotationCommand = new UpdateAnnotationCommand(updatedAnnotationId, updatedAnnotationMemory.toJSON());
				
				dataServer.requestSendCommand(updateAnnotationCommand);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Log.e(TAG, "ERROR: tried to send update annotation, but dataServer was null");
		}
	}
	
	public static void sendAnnotations(MainActivity delegate) {
		Log.d(TAG, "sendAnnotations()");
		
		DataServer dataServer = delegate.getDataServer();
		if (dataServer != null) {

			try {				
				JSONObject annotationStateObject = AnnotationStates.getInstance().getGlobalState().toJSON();
								
				SendAnnotationsCommand sendAnnotationsCommand = new SendAnnotationsCommand(annotationStateObject);
				
				dataServer.requestSendCommand(sendAnnotationsCommand);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Log.e(TAG, "ERROR: tried to send annotations, but dataServer was null");
		}
	}
	

	
}
