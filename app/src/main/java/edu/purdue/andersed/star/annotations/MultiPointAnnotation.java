package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class MultiPointAnnotation extends Annotation {

	private static final String TAG = "MultiPointAnnotation";
	
	private static final int NUM_POINTS_IN_CIRCLE = 8;

	public MultiPointAnnotation(MatOfPoint2f pointsMat) {
		mPointData.setPointsMat(pointsMat);
	}
	
	public MultiPointAnnotation() {
		Log.d(TAG, "creating empty MultiPointAnnotation");
	}

	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		drawWithColorAndThickness(pointOverlay, color, Pref.getInstance().getAnnotationLineThickness());
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		int circleRadiusPx = Pref.getInstance().getCircleAnnotationRadiusPixels();

		List<Point> screenSpacePoints = mPointData.getScreenSpacePoints();
		for (Point screenSpacePoint : screenSpacePoints) {
            PointData pointsForThisCircle = new PointData();

            for (int i = 0; i <= NUM_POINTS_IN_CIRCLE; i++) {
                double radians = i * (2.0 * Math.PI) / NUM_POINTS_IN_CIRCLE;
                Point nextPointInCircle = new Point(
                        screenSpacePoint.x + circleRadiusPx * Math.cos(radians),
                        screenSpacePoint.y + circleRadiusPx * Math.sin(radians)
                );
                pointsForThisCircle.addScreenSpacePoint(nextPointInCircle);
            }
            pointOverlay.drawLinesWithColorAndThickness(pointsForThisCircle, color, thickness, true, -1);
		}

	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_MULTI_POINT;
	}

	@Override
	String getGeometryString() {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		return pointsInScreenSpace.toString();		
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject multiPointAnnotationObject = new JSONObject();
		
		multiPointAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		multiPointAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(mPointData.getPointsMat()));
		
		//Log.d(TAG, "result: " + multiPointAnnotationObject.toString());
		return multiPointAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		MultiPointAnnotation multiPointAnnotation = new MultiPointAnnotation(pointMat);
		
		return multiPointAnnotation;
	}

	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}

	

}
