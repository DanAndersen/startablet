package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.Toaster;
import edu.purdue.andersed.star.settings.Pref;

public class AnnotationStateLogging {

	private static final String TAG = "AnnotationStateLogging";
	
	private static AnnotationStateLogging instance;
	
	private int mFrameNumber;
	private long mTimestamp;

	private File mGlobalLogFile;
	private File mLocalLogFile;

	private BufferedWriter mGlobalBufferedWriter;
	private BufferedWriter mLocalBufferedWriter;
	
	private static final String LOG_PREFIX = "Frame\tTimestamp\tAnnotationState";
	
	public static AnnotationStateLogging getInstance() {
		if (instance == null) {
			instance = new AnnotationStateLogging();
			try {
				instance.initLogging();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toaster.getInstance().toast("couldn't set up annotation state logging");
			}
		}
		return instance;
	}
	
	public void initLogging() throws IOException {
		Log.d(TAG, "initLogging()");
		
		mFrameNumber = 0;
		
		mTimestamp = System.currentTimeMillis();

		STARUtils.createLogDirIfNotExist();
		String globalPath = Pref.getInstance().getLogDir() + "/STAR_global_annotation_state_log_" + mTimestamp + ".log";
		String localPath = Pref.getInstance().getLogDir() + "/STAR_local_annotation_state_log_" + mTimestamp + ".log";
		
		Log.d(TAG, "going to create log file at " + globalPath);

		mGlobalLogFile = new File(globalPath);
		if (!mGlobalLogFile.exists()) {
			mGlobalLogFile.createNewFile();
		}

		Log.d(TAG, "going to create log file at " + localPath);

		mLocalLogFile = new File(globalPath);
		if (!mLocalLogFile.exists()) {
			mLocalLogFile.createNewFile();
		}

		Log.d(TAG, "setting up bufferedwriter for log");
		mGlobalBufferedWriter = new BufferedWriter(new FileWriter(mGlobalLogFile, true));
		mLocalBufferedWriter = new BufferedWriter(new FileWriter(mLocalLogFile, true));
		
		writeLogPrefix();
		
	}
	
	private void writeLogPrefix() throws IOException {
		mGlobalBufferedWriter.append(LOG_PREFIX);
		mGlobalBufferedWriter.newLine();
		mGlobalBufferedWriter.flush();

		mLocalBufferedWriter.append(LOG_PREFIX);
		mLocalBufferedWriter.newLine();
		mLocalBufferedWriter.flush();
	}
	
	public void writeLogForFrame() {
		try {
			mFrameNumber++;
			
			long currentTimestamp = System.currentTimeMillis();
			
			JSONObject currentGlobalAnnotationState = AnnotationStates.getInstance().getGlobalState().toSimpleJSON();
			JSONObject currentLocalAnnotationState = AnnotationStates.getInstance().getLocalState().toSimpleJSON();
			
			String globalLogLine = mFrameNumber + "\t" + currentTimestamp + "\t" + currentGlobalAnnotationState.toString();
			String localLogLine = mFrameNumber + "\t" + currentTimestamp + "\t" + currentLocalAnnotationState.toString();

			mGlobalBufferedWriter.append(globalLogLine);
			mGlobalBufferedWriter.newLine();
			mGlobalBufferedWriter.flush();

			mLocalBufferedWriter.append(localLogLine);
			mLocalBufferedWriter.newLine();
			mLocalBufferedWriter.flush();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeLog() {
		Log.d(TAG, "closeLog");
		
		if (mGlobalBufferedWriter != null) {
			try {
				mGlobalBufferedWriter.flush();
				mGlobalBufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		mGlobalLogFile = null;



		if (mLocalBufferedWriter != null) {
			try {
				mLocalBufferedWriter.flush();
				mLocalBufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		mLocalLogFile = null;
	}
}
