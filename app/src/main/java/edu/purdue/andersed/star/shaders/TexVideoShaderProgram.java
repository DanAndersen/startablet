package edu.purdue.andersed.star.shaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES30;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;

import org.opencv.core.Point;

import java.nio.IntBuffer;
import java.util.EnumMap;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.annotations.AnimationAnnotation;
import edu.purdue.andersed.star.annotations.ToolAnnotation;
import edu.purdue.andersed.star.annotations.ToolTemplate;
import edu.purdue.andersed.star.annotations.ToolType;
import edu.purdue.andersed.star.geometry.Quad;
import edu.purdue.andersed.star.settings.Pref;

public class TexVideoShaderProgram {

    private static final String TAG = "TexVideoShaderProgram";

    private static final String VERTEX_SHADER =
            "attribute vec4 vertexPosition; \n" +
                    "uniform mat4 uMVPMatrix; \n" +
                    "attribute vec2 vertexTexCoord; \n" +
                    "varying vec2 texCoord; \n" +
                    "void main() \n" +
                    "{ \n" +
                    "    gl_Position = uMVPMatrix * vertexPosition; \n" +

                    "    texCoord = vertexTexCoord; \n" +
                    "} \n"
            ;

    private static final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float; \n" +
                    "varying vec2 texCoord; \n" +
                    "uniform samplerExternalOES videoTexSampler; \n" +
                    "uniform float toolTransparency; \n" +
                    "uniform float brightnessOffset; \n" +
                    "uniform float contrastOffset; \n" +
                    "uniform bool isSelected; \n" +
                    "uniform bool drawingSelectableLayer; \n" +
                    "uniform vec4 toolColor; \n" +
                    " \n" +
                    "vec3 rgb2hsv(vec3 c) \n" +
                    "{ \n" +
                    "    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0); \n" +
                    "    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g)); \n" +
                    "    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r)); \n" +
                    " \n" +
                    "    float d = q.x - min(q.w, q.y); \n" +
                    "    float e = 1.0e-10; \n" +
                    "    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x); \n" +
                    "} \n" +
                    " \n" +
                    "vec3 hsv2rgb(vec3 c) \n" +
                    "{ \n" +
                    "    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0); \n" +
                    "    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www); \n" +
                    "    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y); \n" +
                    "} \n" +
                    " \n" +
                    "void main() \n" +
                    "{ \n" +
                    "    vec4 textureColor = texture2D(videoTexSampler, texCoord); \n" +
                    "    vec3 textureRGB = textureColor.rgb; \n" +
                    "    float alpha = textureColor.a; \n" +
                    "    vec3 textureHSV = rgb2hsv(textureRGB); \n" +
                    "    textureHSV.y += contrastOffset; \n" +
                    "    textureHSV.z += brightnessOffset; \n" +
                    "    vec3 adjustedTextureRGB = hsv2rgb(textureHSV); \n" +
                    "    float threshold = 30.0 / 255.0; \n" +
                    "    if (textureColor.r <= threshold && textureColor.g <= threshold && textureColor.b <= threshold) { \n" +
                    "        float meanRGB = (textureColor.r + textureColor.g + textureColor.b) / 3.0; \n" +
                    "        float minThreshold = 0.0; \n" +
                    "        alpha = clamp((meanRGB - minThreshold) / (threshold - minThreshold), 0.0, 1.0); \n" +
                    "        \n" +
                    "        \n" +
                    "    } \n" +
                    "    if (drawingSelectableLayer) { \n" +
                    "        if (alpha <= 0.0) { \n" +
                    "            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0); \n" +
                    "        }  else { \n" +
                    "            gl_FragColor = toolColor; \n" +
                    "        } \n" +
                    "    } else { \n" +
                    "        if (isSelected) { " +
                    "            adjustedTextureRGB = mix(adjustedTextureRGB, vec3(1.0, 0.0, 0.0), 0.5); " +
                    "        } " +
                    "        gl_FragColor = vec4(adjustedTextureRGB, alpha - toolTransparency); \n" +
                    "    } \n" +
                    "} \n"
            ;

    private int mShaderProgramID;

    private int mVertexHandle;

    private int mMVPMatrixHandle;

    private int mTextureCoordHandle;
    private int mVideoTexSamplerHandle;
    private int mToolTransparencyHandle;
    private int mIsSelectedHandle;
    private int mDrawingSelectableLayerHandle;
    private int mToolColorHandle;

    private int mBrightnessOffsetHandle;
    private int mContrastOffsetHandle;

    public static float BrightnessOffset = 0.0f;
    public static float ContrastOffset = 0.0f;
    public static float ToolTransparency = 0.5f;

    public TexVideoShaderProgram(Context context) {
        setupShaderProgram(context);
    }

    private void setupShaderProgram(Context context) {
        Log.d(TAG, "creating shader program");

        Log.d(TAG, "creating geometry");

        mShaderProgramID = STARUtils.createProgramFromShaderSrc(VERTEX_SHADER, FRAGMENT_SHADER);
        STARUtils.checkGLError("createProgramFromShaderSrc");

        Log.d(TAG, "setting up handles");
        mVertexHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexPosition");
        STARUtils.checkGLError("glGetAttribLocation");

        mMVPMatrixHandle = GLES30.glGetUniformLocation(mShaderProgramID, "uMVPMatrix");
        STARUtils.checkGLError("glGetUniformLocation");
        mToolTransparencyHandle = GLES30.glGetUniformLocation(mShaderProgramID, "toolTransparency");
        STARUtils.checkGLError("glGetUniformLocation");

        mBrightnessOffsetHandle = GLES30.glGetUniformLocation(mShaderProgramID, "brightnessOffset");
        STARUtils.checkGLError("glGetUniformLocation");

        mContrastOffsetHandle = GLES30.glGetUniformLocation(mShaderProgramID, "contrastOffset");
        STARUtils.checkGLError("glGetUniformLocation");

        mIsSelectedHandle = GLES30.glGetUniformLocation(mShaderProgramID, "isSelected");
        STARUtils.checkGLError("glGetUniformLocation");

        mDrawingSelectableLayerHandle = GLES30.glGetUniformLocation(mShaderProgramID, "drawingSelectableLayer");
        STARUtils.checkGLError("glGetUniformLocation");

        mToolColorHandle = GLES30.glGetUniformLocation(mShaderProgramID, "toolColor");
        STARUtils.checkGLError("glGetUniformLocation");

        mTextureCoordHandle = GLES30.glGetAttribLocation(mShaderProgramID, "vertexTexCoord");
        STARUtils.checkGLError("glGetAttribLocation");

        mVideoTexSamplerHandle = GLES30.glGetAttribLocation(mShaderProgramID, "videoTexSampler");
        STARUtils.checkGLError("glGetAttribLocation");

        setupToolTemplateData(context);
    }

    public void setupToolTemplateData(Context context) {
        Log.d(TAG, "setting up the geometries and textures for each tool type");
        for (ToolType tt : ToolType.values()) {
            ToolTextureStorage.getInstance().setupTextureAndGeometryForToolTemplate(context, tt);
        }
    }

    public void teardownToolTemplateData(Context context) {
        Log.d(TAG, "tearing down the geometries and textures for each tool type");
        for (ToolType tt : ToolType.values()) {
            ToolTextureStorage.getInstance().teardownTextureAndGeometryForToolTemplate(context, tt);
        }
    }

    public void drawTool(Context context, AnimationAnnotation animationAnnotation, boolean isSelected, boolean drawingSelectableLayer) {
        if (!animationAnnotation.isMediaPlayerInitialized()) {
            Log.d(TAG, "media player is not initialized for animation annotation, starting it");
            animationAnnotation.startMediaPlayer(context);
        }

        // now that the mediaplayer is (hopefully) initialized, update the tex image
        if (animationAnnotation.isMediaPlayerInitialized()) {
            if (animationAnnotation.isActive()) {

                if (!animationAnnotation.isPlayingCorrectVideo()) {
                    Log.e(TAG, "caught the animation annotation playing the wrong video clip, restarting the media player");
                    animationAnnotation.startMediaPlayer(context);
                }

                ToolType toolType = animationAnnotation.getToolType();

                SurfaceTexture st = ToolTextureStorage.getInstance().getSurfaceTexture(toolType);
                if (st != null) {
                    try {
                        st.updateTexImage();
                    } catch (IllegalStateException e) {
                        Log.e(TAG, e.getMessage());
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "not updating tex image in surface texture, because surfacetexture is null");
                }
            } else {
                Log.e(TAG, "not updating tex image because animation annotation is not marked as active");
            }
        } else {
            Log.e(TAG, "not updating tex image because animation annotation does not have media player initialized");
        }


        ToolType toolType = animationAnnotation.getToolType();

        Point anchorPoint = animationAnnotation.getPointInScreenSpace();

        if (anchorPoint != null) {


            Quad geometry = ToolTextureStorage.getInstance().getGeometry(toolType);
            Integer videoTextureID = ToolTextureStorage.getInstance().getVideoTextureID(toolType);

            if (geometry != null) {
                if (videoTextureID != null) {
                    GLES30.glUseProgram(mShaderProgramID);
                    STARUtils.checkGLError("glUseProgram");

                    GLES30.glVertexAttribPointer(mVertexHandle, 3, GLES30.GL_FLOAT, false, 0, geometry.getVertices());
                    STARUtils.checkGLError("glVertexAttribPointer");
                    GLES30.glVertexAttribPointer(mTextureCoordHandle, 2, GLES30.GL_FLOAT, false, 0, geometry.getTexCoords());
                    STARUtils.checkGLError("glVertexAttribPointer");
                    GLES30.glEnableVertexAttribArray(mVertexHandle);
                    STARUtils.checkGLError("glEnableVertexAttribArray");
                    GLES30.glEnableVertexAttribArray(mTextureCoordHandle);
                    STARUtils.checkGLError("glEnableVertexAttribArray");



                    GLES30.glActiveTexture(GLES30.GL_TEXTURE1);
                    STARUtils.checkGLError("glActiveTexture");
                    GLES30.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
                            videoTextureID);
                    STARUtils.checkGLError("glBindTexture");
                    GLES30.glUniform1i(mVideoTexSamplerHandle, 1);
                    STARUtils.checkGLError("glUniform1i");



                    GLES30.glUniform1f(mBrightnessOffsetHandle, BrightnessOffset);
                    STARUtils.checkGLError("glUniform1f");

                    GLES30.glUniform1f(mContrastOffsetHandle, ContrastOffset);
                    STARUtils.checkGLError("glUniform1f");

                    GLES30.glUniform1f(mToolTransparencyHandle, ToolTransparency);
                    STARUtils.checkGLError("glUniform1f");

                    GLES30.glUniform1i(mDrawingSelectableLayerHandle, drawingSelectableLayer ? 1 : 0);
                    STARUtils.checkGLError("glUniform1i");

                    GLES30.glUniform1i(mIsSelectedHandle, isSelected ? 1 : 0);
                    STARUtils.checkGLError("glUniform1i");

                    float[] mvpMatrix = new float[16];

                    double degrees = animationAnnotation.getDegrees();
                    double scale = animationAnnotation.getScale();
                    int color = animationAnnotation.getSelectableColor();

                    //Log.d(TAG, "degrees: " + degrees);
                    //Log.d(TAG, "scale: " + scale);

                    Matrix.setIdentityM(mvpMatrix, 0);

                    Matrix.translateM(mvpMatrix, 0,
                            (float)(2 * (anchorPoint.x - ScreenState.getInstance().getHalfScreenWidth()) / (float) ScreenState.getInstance().getScreenWidth()),
                            (float)(-2 * (anchorPoint.y - ScreenState.getInstance().getHalfScreenHeight()) / (float) ScreenState.getInstance().getScreenHeight()),
                            0.0f);

                    Matrix.scaleM(mvpMatrix, 0, 1.0f, ScreenState.getInstance().getAspectRatio(), 1.0f);

                    Matrix.scaleM(mvpMatrix, 0, (float) scale, (float) scale, 1.0f);

                    Matrix.rotateM(mvpMatrix, 0, (float) degrees, 0.0f, 0.0f, 1.0f);

                    Matrix.translateM(mvpMatrix, 0, geometry.getScaledOffsetFromCenterX(), geometry.getScaledOffsetFromCenterY(), 0.0f);

                    float r = (float) Color.red(color) / 255;
                    float g = (float) Color.green(color) / 255;
                    float b = (float) Color.blue(color) / 255;

                    //Log.d(TAG, "color: " + r + " " + g + " " + b);

                    GLES30.glUniform4f(mToolColorHandle, r, g, b, 255);
                    STARUtils.checkGLError("glUniform4f");

                    GLES30.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
                    STARUtils.checkGLError("glUniformMatrix4fv");

                    GLES30.glDrawElements(GLES30.GL_TRIANGLES,
                            geometry.getNumObjectIndex(), GLES30.GL_UNSIGNED_SHORT,
                            geometry.getIndices());
                    STARUtils.checkGLError("glDrawElements");
                    GLES30.glDisableVertexAttribArray(mVertexHandle);
                    STARUtils.checkGLError("glDisableVertexAttribArray");
                    GLES30.glDisableVertexAttribArray(mTextureCoordHandle);
                    STARUtils.checkGLError("glDisableVertexAttribArray");
                } else {
                    Log.e(TAG, "tried to draw tex video but videoTextureID was null");
                }
            } else {
                Log.e(TAG, "tried to draw tex video but quad geometry was null");
            }
        } else {
            Log.e(TAG, "anchor point for drawing tool was null");
        }



    }
}
