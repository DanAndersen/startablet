package edu.purdue.andersed.star.videosource;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.Surface;

import java.io.IOException;

import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.settings.Pref;

/**
 * Created by andersed on 2/8/2017.
 */

public class ScriptedSequenceVideoSource extends VideoSource implements MediaPlayer.OnCompletionListener {

    private static final String TAG = "ScriptedSequence";

    private MainActivity mDelegate;

    private ScriptedVideoSequence mVideoSequence;

    private int mCurrentVideoStageIndex;
    private String mCurrentVideoStageAssetFilename;

    private boolean mMediaPlayerInitialized;
    private MediaPlayer mMediaPlayer;

    private boolean mMediaPlayerIsPrepared;

    private SurfaceTexture mSurfaceTexture;

    private boolean mIsActive; // false if the surface is detached from the context

    public ScriptedSequenceVideoSource(MainActivity delegate) {
        mDelegate = delegate;

        mVideoSequence = ScriptedVideoSequence.PAPER_LINES;

        mCurrentVideoStageIndex = 0;
    }

    public void advanceVideoStage() {
        int newVideoStageIndex = mCurrentVideoStageIndex + 1;
        if (mCurrentVideoStageIndex >= mVideoSequence.getStages().size()) {
            newVideoStageIndex = 0;
        }

        mCurrentVideoStageIndex = newVideoStageIndex;

        // invalidate so we reconstruct with the right video
        mCurrentVideoStageAssetFilename = "";
        mMediaPlayerInitialized = false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        ScriptedVideoStage currentStage = mVideoSequence.getStages().get(mCurrentVideoStageIndex);

        if (currentStage.shouldLoop()) {
            mMediaPlayer.start(); // start the current media player again
        } else {
            // automatically progress to the next stage in the list. If we are at the end, just loop it

            advanceVideoStage();
        }
    }

    public boolean isPlayingCorrectVideo() {
        synchronized (this) {
            if (mCurrentVideoStageIndex < 0 || mCurrentVideoStageIndex >= mVideoSequence.getStages().size()) {
                return false;
            } else {
                String correctVideoAssetFilename = mVideoSequence.getStages().get(mCurrentVideoStageIndex).getVideoAssetPath();
                return (correctVideoAssetFilename.equals(mCurrentVideoStageAssetFilename));
            }
        }
    }

    public void startMediaPlayer(Context context) {
        synchronized (this) {
            Log.d(TAG, "begin startMediaPlayer");

            SurfaceTexture surfaceTexture = mSurfaceTexture;
            if (surfaceTexture == null) {
                Log.e(TAG, "attempted to start media player for this animation annotation, but the surface texture is null. do nothing.");
                return;
            }


            if (mMediaPlayer != null) {
                Log.d(TAG, "mMediaPlayer already exists, stopping and resetting");
                if (mMediaPlayerIsPrepared) {
                    mMediaPlayer.stop();
                    mMediaPlayerIsPrepared = false;
                }
                mMediaPlayer.setSurface(null);
                mMediaPlayer.reset();
            } else {
                Log.d(TAG, "initing mMediaPlayer");
                mMediaPlayer = new MediaPlayer();
            }



            String videoAssetFilename;
            if (mCurrentVideoStageIndex >= 0 && mCurrentVideoStageIndex < mVideoSequence.getStages().size()) {
                videoAssetFilename = mVideoSequence.getStages().get(mCurrentVideoStageIndex).getVideoAssetPath();
            } else {
                videoAssetFilename = mVideoSequence.getStages().get(0).getVideoAssetPath();
            }

            try {
                AssetFileDescriptor afd = context.getAssets().openFd(videoAssetFilename);
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mCurrentVideoStageAssetFilename = videoAssetFilename;

                Surface surface = new Surface(surfaceTexture);
                mMediaPlayer.setSurface(surface);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.setScreenOnWhilePlaying(true);
                mMediaPlayer.setOnCompletionListener(this);
                surface.release();

                Log.d(TAG, "preparing mMediaPlayer");
                mMediaPlayer.prepare();
                mMediaPlayerIsPrepared = true;

                Log.d(TAG, "starting mMediaPlayer");
                mMediaPlayer.start();

                mMediaPlayerInitialized = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            mIsActive = true;
            Log.d(TAG, "end startMediaPlayer");
        }
    }

    public boolean isMediaPlayerInitialized() {
        return mMediaPlayerInitialized;
    }

    @Override
    public void initAndStartVideoSource(SurfaceTexture surfaceTexture) {
        mSurfaceTexture = surfaceTexture;

        startMediaPlayer(mDelegate);
    }

    @Override
    public void stopVideoSource() {
        teardownMediaPlayer();
    }

    private void teardownMediaPlayer() {
        synchronized (this) {
            Log.d(TAG, "begin onDestroy");
            if (mMediaPlayer != null) {
                Log.d(TAG, "stopping and resetting mMediaPlayer");
                if (mMediaPlayerIsPrepared) {
                    mMediaPlayer.stop();
                    mMediaPlayerIsPrepared = false;
                }
                mMediaPlayer.setSurface(null);
                mMediaPlayer.reset();
                mMediaPlayer = null;
            }
            mMediaPlayerInitialized = false;
            mIsActive = false;
            Log.d(TAG, "end onDestroy");
        }
    }

    public boolean isActive() {
        return mIsActive;
    }
}
