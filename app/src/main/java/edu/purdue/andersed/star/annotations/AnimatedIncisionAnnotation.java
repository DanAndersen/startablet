package edu.purdue.andersed.star.annotations;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class AnimatedIncisionAnnotation extends PolylineAnnotation {

	private static final String TAG = "AnimatedIncisionAnnotation";
	
	private static final int ANIMATION_MILLIS = 3000;
	
	private ToolAnnotation mIllustrationScalpel;
	
	@Override
	String getAnnotationType() {
		return Annotation.TYPE_ANIMATED_INCISION;
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		
		double elapsedMillis = System.currentTimeMillis() % ANIMATION_MILLIS;
		double animationProgress = elapsedMillis / ANIMATION_MILLIS; // from 0.0 to 1.0
		
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		int endPointIndex = (int) Math.round(pointsInScreenSpace.size() * animationProgress);

        pointOverlay.drawLinesWithColorAndThickness(mPointData, color, thickness, false, endPointIndex);

        if (endPointIndex >= 2) {
            // need at least 2 points to get tangent line to be able to draw the scalpel
            List<Point> endpointScreenSpace = new ArrayList<>();
            endpointScreenSpace.add(pointsInScreenSpace.get(endPointIndex-1));
            mIllustrationScalpel.setPointsInScreenSpace(endpointScreenSpace);

            Point p1 = pointsInScreenSpace.get(endPointIndex-2);
            Point p2 = pointsInScreenSpace.get(endPointIndex-1);

            double radians = Math.atan2(p2.y-p1.y, p2.x-p1.x);

            double degrees = STARUtils.radiansToDegrees(radians);

            mIllustrationScalpel.setDegrees(degrees);

            pointOverlay.drawTool(mIllustrationScalpel);
        }


	}
	
	public AnimatedIncisionAnnotation(MatOfPoint2f pointMat) {
		super(pointMat);
		initScalpel();
	}
	
	public AnimatedIncisionAnnotation() {
		super();
		initScalpel();
	}
	
	private void initScalpel() {
		mIllustrationScalpel = new ToolAnnotation(new Point(0,0), ToolType.SCALPEL, 45, 0.3, STARUtils.getRandomColor());
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		AnimatedIncisionAnnotation polylineAnnotation = new AnimatedIncisionAnnotation(pointMat);
		
		return polylineAnnotation;
	}
}
