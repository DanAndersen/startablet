package edu.purdue.andersed.star.annotations;

import java.util.List;

/**
 * Created by andersed on 11/8/2016.
 */

public class AnimationTemplate extends ToolTemplate {

    // each animation is composed of multiple stages, each of which has some label and some associated filename
    private final List<String> mVideoStageLabels;
    private final List<String> mVideoDescriptionLabels;
    private final List<String> mVideoStageAssetFilenames;

    public AnimationTemplate(int previewIconDrawableResourceId, int width, int height, int anchorX, int anchorY,
                             List<String> videoStageLabels,
                             List<String> videoDescriptionLabels,
                             List<String> videoStageAssetFilenames) {
        super(previewIconDrawableResourceId, width, height, anchorX, anchorY);

        assert (videoStageLabels.size() == videoStageAssetFilenames.size());
        assert (videoStageLabels.size() == videoDescriptionLabels.size());

        mVideoStageLabels = videoStageLabels;
        mVideoDescriptionLabels = videoDescriptionLabels;
        mVideoStageAssetFilenames = videoStageAssetFilenames;
    }

    public AnimationTemplate(String previewIconAssetFilename, int width, int height, int anchorX, int anchorY, List<String> videoStageLabels,
                             List<String> videoDescriptionLabels,
                             List<String> videoStageAssetFilenames) {
        super(previewIconAssetFilename, width, height, anchorX, anchorY);

        assert (videoStageLabels.size() == videoStageAssetFilenames.size());
        assert (videoStageLabels.size() == videoDescriptionLabels.size());

        mVideoStageLabels = videoStageLabels;
        mVideoDescriptionLabels = videoDescriptionLabels;
        mVideoStageAssetFilenames = videoStageAssetFilenames;
    }

    public List<String> getVideoStageAssetFilenames() {
        return mVideoStageAssetFilenames;
    }

    public List<String> getVideoStageLabels() {
        return mVideoStageLabels;
    }

    public List<String> getVideoDescriptionLabels() {
        return mVideoDescriptionLabels;
    }

    @Override
    public Class<?> getAnnotationClass() {
        return AnimationAnnotation.class;
    }
}
