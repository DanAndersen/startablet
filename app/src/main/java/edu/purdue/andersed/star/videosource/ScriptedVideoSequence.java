package edu.purdue.andersed.star.videosource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andersed on 2/8/2017.
 */

public class ScriptedVideoSequence {

    public static final ScriptedVideoSequence PAPER_LINES;
    static {
        PAPER_LINES = new ScriptedVideoSequence();
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/loop_before.mp4", true);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/do_step_0.mp4", false);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/loop_0.mp4", true);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/do_step_1.mp4", false);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/loop_1.mp4", true);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/do_step_2.mp4", false);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/loop_2.mp4", true);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/do_step_3.mp4", false);
        PAPER_LINES.addClip("mock_camera_sources/paper_lines/loop_3.mp4", true);
    }



    private List<ScriptedVideoStage> stages;

    public ScriptedVideoSequence() {
        stages = new ArrayList<>();
    }

    public void addClip(String videoAssetPath, boolean looping) {
        stages.add(new ScriptedVideoStage(videoAssetPath, looping));
    }

    public List<ScriptedVideoStage> getStages() {
        return stages;
    }
}

