package edu.purdue.andersed.star.ipconnect;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.Socket;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.annotations.AnnotationMemory;
import edu.purdue.andersed.star.annotations.AnnotationState;
import edu.purdue.andersed.star.annotations.AnnotationStates;
import edu.purdue.andersed.star.commands.Command;
import edu.purdue.andersed.star.commands.CreateAnnotationCommand;
import edu.purdue.andersed.star.commands.DeleteAnnotationCommand;
import edu.purdue.andersed.star.commands.SendAnnotationsCommand;
import edu.purdue.andersed.star.commands.UpdateAnnotationCommand;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class DataClient {

	// used by the trainee system, establishes a connection to the mentor system and supports receiving from the mentor server
	
	private static final String TAG = "DataClient";
	
	private Thread mDataClientLoopThread;

    private boolean mRunning;

	public DataClient() {
		Log.d(TAG, "constructing DataClient");
	}

    private void teardownIfNeeded() {
        Log.d(TAG, "teardownIfNeeded");
        mRunning = false;

        if (mDataClientLoopThread != null && mDataClientLoopThread.isAlive()) {
            Log.d(TAG, "mDataClientLoopThread is alive, interrupting");
            mDataClientLoopThread.interrupt();
        }
    }

	public void init() {
		Log.d(TAG, "initializing DataClient");

        teardownIfNeeded();

        mDataClientLoopThread = new Thread(new DataClientLoop());

        mRunning = true;

        mDataClientLoopThread.start();
	}
	
	public void destroy() {
		Log.d(TAG, "destroying DataClient");
        teardownIfNeeded();
	}

    private class DataClientLoop implements Runnable {
        private static final String TAG = "DataClientLoop";

        private Socket mSocket;

        public void run() {
            Log.d(TAG, "starting run()");

            try {
                while (mRunning && !Thread.currentThread().isInterrupted()) {

                    try {

                        String host = PeerConnection.getInstance().getMentorIP();
                        int port = Pref.getInstance().getDataServerSocketPort();

                        Log.d(TAG, "constructing new socket");
                        mSocket = new Socket(host, port);
                        Log.d(TAG, "constructed new socket");
                        InputStream inputStream = mSocket.getInputStream();
                        InputStreamReader in = new InputStreamReader(inputStream);
                        BufferedReader bf = new BufferedReader(in);

                        Log.d(TAG, "started socket");

                        while (mRunning && !Thread.currentThread().isInterrupted() && mSocket.isConnected()) {

                            String inputLine = bf.readLine();
                            Log.d(TAG, "inputLine: " + inputLine);

                            if (inputLine == null) {
                                break;
                            } else {
                                StringBuilder incomingCommandStringBuilder = new StringBuilder();
                                Log.d(TAG, "received inputLine: " + inputLine);
                                incomingCommandStringBuilder.append(inputLine);

                                Log.d(TAG, "received incoming command string");
                                STARUtils.saveStringToFile(incomingCommandStringBuilder.toString(), "star_received_command_" + System.currentTimeMillis() + ".log");

                                JSONObject incomingCommandJsonObject = new JSONObject(incomingCommandStringBuilder.toString());
                                Log.d(TAG, "got incoming json object: " + incomingCommandJsonObject.toString());

                                Command incomingCommand = Command.commandFromJSON(incomingCommandJsonObject);
                                processCommand(incomingCommand);
                            }
                        }
                    } catch (ConnectException e) {
                        Log.d(TAG, e.getMessage());
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    int retryMillis = Pref.getInstance().getClientRetryMillis();
                    Log.d(TAG, "data client connection closed. waiting for " + retryMillis + " ms to retry.");
                    Thread.sleep(retryMillis);
                }

            } catch (InterruptedException e) {
                Log.d(TAG, "Interrupted: " + e.getMessage());
            }
            Log.d(TAG, "ending run()");
        }
    }

	private void processCommand(Command command) {
    	Log.d(TAG, "processCommand()");
    	
		if (command instanceof SendAnnotationsCommand) {
			SendAnnotationsCommand sendAnnotationsCommand = (SendAnnotationsCommand) command;
			
			Log.d(TAG, "got a JSON representation of the annotation state");
			
			try {
				Log.d(TAG, "updating annotation state with received JSON representation");
                AnnotationStates.getInstance().getGlobalState().updateFromJSONObject(sendAnnotationsCommand.getAnnotationStateObject());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (command instanceof CreateAnnotationCommand) {
			CreateAnnotationCommand createAnnotationCommand = (CreateAnnotationCommand) command;
			
			Log.d(TAG, "got a command to create a new annotation");
			
			try {
				Integer newAnnotationId = createAnnotationCommand.getId();
				AnnotationMemory newAnnotationMemory = AnnotationMemory.fromJSON(createAnnotationCommand.getAnnotationMemoryJSONObject());
                AnnotationStates.getInstance().getGlobalState().addAnnotationMemoryWithID(newAnnotationId, newAnnotationMemory);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (command instanceof UpdateAnnotationCommand) {
			UpdateAnnotationCommand updateAnnotationCommand = (UpdateAnnotationCommand) command;
			
			Log.d(TAG, "got a command to update an existing annotation");
			
			try {
				Integer updatedAnnotationId = updateAnnotationCommand.getId();

                AnnotationStates.getInstance().getGlobalState()._removeAnnotationFromMap(updatedAnnotationId);

				AnnotationMemory updatedAnnotationMemory = AnnotationMemory.fromJSON(updateAnnotationCommand.getAnnotationMemoryJSONObject());
                AnnotationStates.getInstance().getGlobalState().updateAnnotationMemoryWithID(updatedAnnotationId, updatedAnnotationMemory);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else if (command instanceof DeleteAnnotationCommand) {
			DeleteAnnotationCommand deleteAnnotationCommand = (DeleteAnnotationCommand) command;
			
			Log.d(TAG, "got a command to delete an annotation");
			
			Integer annotationId = deleteAnnotationCommand.getId();

            AnnotationStates.getInstance().getGlobalState()._removeAnnotationFromMap(annotationId);
		} else {
			Log.e(TAG, "unrecognized command type");
		}
	}
}
