package edu.purdue.andersed.star.shaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES30;
import android.opengl.GLUtils;
import android.util.Log;
import android.view.Surface;

import java.util.EnumMap;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.annotations.AnimationTemplate;
import edu.purdue.andersed.star.annotations.ToolTemplate;
import edu.purdue.andersed.star.annotations.ToolType;
import edu.purdue.andersed.star.geometry.Quad;

/**
 * Created by andersed on 11/4/2016.
 */

public class ToolTextureStorage {

    private static final String TAG = "ToolTextureStorage";

    private static ToolTextureStorage instance;

    private EnumMap<ToolType, Integer> mSpriteTextureIDs;
    private EnumMap<ToolType, Integer> mVideoTextureIDs;
    private EnumMap<ToolType, Quad> mGeometries;
    private EnumMap<ToolType, SurfaceTexture> mSurfaceTextures; // surface textures for playing videos

    public static ToolTextureStorage getInstance() {
        if (instance == null) {
            instance = new ToolTextureStorage();
        }
        return instance;
    }

    private ToolTextureStorage() {
        mSpriteTextureIDs = new EnumMap<ToolType, Integer>(ToolType.class);
        mVideoTextureIDs = new EnumMap<ToolType, Integer>(ToolType.class);
        mGeometries = new EnumMap<ToolType, Quad>(ToolType.class);
        mSurfaceTextures = new EnumMap<ToolType, SurfaceTexture>(ToolType.class);
    }

    public boolean hasSpriteTextureIDForToolType(ToolType tt) {
        return mSpriteTextureIDs.containsKey(tt);
    }

    public boolean hasVideoTextureIDForToolType(ToolType tt) {
        return mVideoTextureIDs.containsKey(tt);
    }

    public boolean hasGeometryForToolType(ToolType tt) {
        return mGeometries.containsKey(tt);
    }

    public void putGeometry(ToolType toolType, Quad quad) {
        mGeometries.put(toolType, quad);
    }

    public void putSpriteTextureID(ToolType toolType, Integer textureID) {
        mSpriteTextureIDs.put(toolType, textureID);
    }

    public Integer getSpriteTextureID(ToolType tt) {
        return mSpriteTextureIDs.get(tt);
    }

    public Integer getVideoTextureID(ToolType tt) {
        return mVideoTextureIDs.get(tt);
    }

    public Quad getGeometry(ToolType tt) {
        return mGeometries.get(tt);
    }

    public SurfaceTexture getSurfaceTexture(ToolType tt) {
        return mSurfaceTextures.get(tt);
    }

    private float MAX_TOOL_SCALE = 1.0f;	// percentage of screen that should be taken up by geometry

    public void teardownTextureAndGeometryForToolTemplate(Context context, ToolType toolType) {
        Log.d(TAG, "start teardownTextureAndGeometryForToolTemplate");

        if (ToolTextureStorage.getInstance().hasGeometryForToolType(toolType)) {
            mGeometries.remove(toolType);
        }

        if (ToolTextureStorage.getInstance().hasSpriteTextureIDForToolType(toolType)) {
            final int[] spriteTextureIDsToDelete = new int[1];
            spriteTextureIDsToDelete[0] = mSpriteTextureIDs.get(toolType);

            GLES30.glDeleteTextures(1, spriteTextureIDsToDelete, 0);

            mSpriteTextureIDs.remove(toolType);
        }

        if (toolType.getToolTemplate() instanceof AnimationTemplate) {
            if (ToolTextureStorage.getInstance().hasVideoTextureIDForToolType(toolType)) {

                final int[] videoTextureIDsToDelete = new int[1];
                videoTextureIDsToDelete[0] = mVideoTextureIDs.get(toolType);

                GLES30.glDeleteTextures(1, videoTextureIDsToDelete, 0);

                mVideoTextureIDs.remove(toolType);
                mSurfaceTextures.remove(toolType);
            }

        }

        Log.d(TAG, "end teardownTextureAndGeometryForToolTemplate");
    }


    public void setupTextureAndGeometryForToolTemplate(Context context, ToolType toolType) {
        Log.d(TAG, "start setupTextureAndGeometryForToolTemplate");
        Bitmap bitmap = null;
        ToolTemplate toolTemplate = toolType.getToolTemplate();

        if (!ToolTextureStorage.getInstance().hasGeometryForToolType(toolType)) {

            if (bitmap == null) {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;   // No pre-scaling

                bitmap = STARUtils.drawableToBitmap(toolTemplate.getDrawable(context));
            }

            float width = bitmap.getWidth();
            float height = bitmap.getHeight();

            Quad quad = new Quad(MAX_TOOL_SCALE, width, height, toolTemplate.getAnchorPixelsFromCenterX(), toolTemplate.getAnchorPixelsFromCenterY());

            ToolTextureStorage.getInstance().putGeometry(toolType, quad);
        }

        if (!ToolTextureStorage.getInstance().hasSpriteTextureIDForToolType(toolType)) {
            if (bitmap == null) {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;   // No pre-scaling

                bitmap = STARUtils.drawableToBitmap(toolTemplate.getDrawable(context));
            }

            final int[] textureHandle = new int[1];

            GLES30.glGenTextures(1, textureHandle, 0);

            if (textureHandle[0] != 0)
            {
                // Read in the resource

                // Bind to the texture in OpenGL
                GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, textureHandle[0]);

                // Set filtering
                GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_NEAREST);
                GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_NEAREST);

                // Load the bitmap into the bound texture.
                GLUtils.texImage2D(GLES30.GL_TEXTURE_2D, 0, bitmap, 0);
            }

            if (textureHandle[0] == 0)
            {
                throw new RuntimeException("Error loading texture.");
            }

            ToolTextureStorage.getInstance().putSpriteTextureID(toolType, textureHandle[0]);
        }

        // Recycle the bitmap, since its data has been loaded into OpenGL.
        /*
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
        */


        // now, if this tool type is for a video animation, create the surface texture for it
        if (toolType.getToolTemplate() instanceof AnimationTemplate) {
            if (!ToolTextureStorage.getInstance().hasVideoTextureIDForToolType(toolType)) {
                int[] videoTexNames = new int[1];

                GLES30.glActiveTexture(GLES30.GL_TEXTURE1);
                GLES30.glGenTextures(1, videoTexNames, 0);
                STARUtils.checkGLError("glGenTextures");

                int videoTexName = videoTexNames[0];

                GLES30.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, videoTexName);
                STARUtils.checkGLError("glBindTexture");

                SurfaceTexture st = new SurfaceTexture(videoTexName);

                mVideoTextureIDs.put(toolType, videoTexName);

                mSurfaceTextures.put(toolType, st);
            }

        }
        Log.d(TAG, "end setupTextureAndGeometryForToolTemplate");
    }
}
