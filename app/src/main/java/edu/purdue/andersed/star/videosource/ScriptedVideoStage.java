package edu.purdue.andersed.star.videosource;

/**
 * Created by andersed on 2/8/2017.
 */

public class ScriptedVideoStage {
    private String videoAssetPath;
    private boolean looping;

    public ScriptedVideoStage(String v, boolean loop) {
        videoAssetPath = v;
        looping = loop;
    }

    public String getVideoAssetPath() {
        return videoAssetPath;
    }

    public boolean shouldLoop() {
        return looping;
    }
}