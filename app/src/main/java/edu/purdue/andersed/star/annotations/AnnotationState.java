package edu.purdue.andersed.star.annotations;

import android.content.Context;
import android.util.Log;
import android.view.animation.Animation;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import edu.purdue.andersed.star.MainActivity;
import edu.purdue.andersed.star.PreviousStepsState;
import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.NetworkUtil;
import edu.purdue.andersed.star.wifidirect.PeerConnection;

public class AnnotationState {

	private static final String TAG = "AnnotationState";

	private Map<Integer, AnnotationMemory> mAnnotationMemoryMap;

	// storing all points internally in OpenCV's image coordinates


	
	public AnnotationState() {
		mAnnotationMemoryMap = new ConcurrentHashMap<Integer, AnnotationMemory>();
	}

	public void onPause() {
        for (AnnotationMemory annotationMemory : mAnnotationMemoryMap.values()) {
            annotationMemory.getAnnotation().onPause();
        }
    }


	public Integer addPointWithScreenCoordinates(Context context, double screenX, double screenY) {
		Log.d(TAG, "addPointWithScreenCoordinates(" + screenX + ", " + screenY + ")");
		
		PointAnnotation pointAnnotation = new PointAnnotation(screenX, screenY);
		
		AnnotationMemory annotationMemory = new AnnotationMemory(pointAnnotation);
				
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}

    public Integer addAnimationWithScreenCoordinatesRotationAndScale(Context context, ToolType toolType, float touchX, float touchY, double radians, double scale, int videoStageIndex) {
        Log.d(TAG, "addAnimationWithScreenCoordinatesRotationAndScale(" + touchX + ", " + touchY + ")");
        Annotation newAnnotation = new AnimationAnnotation(new Point(touchX, touchY), toolType, radians, scale, STARUtils.getRandomColor(), videoStageIndex);
        AnnotationMemory annotationMemory = new AnnotationMemory(newAnnotation);
        Integer id = addAnnotationMemory(annotationMemory);
        return id;
    }

	public Integer addToolWithScreenCoordinatesRotationAndScale(Context context, ToolType toolType, float touchX, float touchY, double radians, double scale) {
		Log.d(TAG, "addToolWithScreenCoordinates(" + touchX + ", " + touchY + ")");
		Annotation newAnnotation = new ToolAnnotation(new Point(touchX, touchY), toolType, radians, scale, STARUtils.getRandomColor());
		AnnotationMemory annotationMemory = new AnnotationMemory(newAnnotation);
		Integer id = addAnnotationMemory(annotationMemory);
		return id;
	}
	
	public void updateToolAnchorPoint(Integer toolAnnotationMemoryID, Point anchorPoint) {
		Log.d(TAG, "updateToolAnchorPoint(" + toolAnnotationMemoryID + ", " + anchorPoint + ")");

		if (toolAnnotationMemoryID != null) {
			
			AnnotationMemory toolAnnotationMemory = this.getAnnotations().get(toolAnnotationMemoryID);
			
			if (toolAnnotationMemory != null) {
				Annotation annotation = toolAnnotationMemory.getAnnotation();
				if (annotation instanceof ToolAnnotation) {
					
					ToolAnnotation toolAnnotation = (ToolAnnotation) annotation;
					List<Point> points = new ArrayList<>();
					points.add(anchorPoint);
					toolAnnotation.setPointsInScreenSpace(points);
				} else {
					Log.e(TAG, "annotation is not a tool annotation");
				}
			} else {
				Log.e(TAG, "cannot update tool annotation when annotation memory is null");
			}
		} else {
			Log.e(TAG, "cannot update tool annotation when id is null");
		}
	}

	public Integer initializeNewPolygonAnnotation(Context context) {
		PolygonAnnotation polygonAnnotation = new PolygonAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(polygonAnnotation);
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer initializeNewAnimatedIncisionAnnotation(Context context) {
		AnimatedIncisionAnnotation animatedIncisionAnnotation = new AnimatedIncisionAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(animatedIncisionAnnotation);
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer initializeNewMultiPointAnnotation(Context context) {
		MultiPointAnnotation multiPointAnnotation = new MultiPointAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(multiPointAnnotation);
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}
	
	public Integer initializeNewPolylineAnnotation(Context context) {
		PolylineAnnotation polylineAnnotation = new PolylineAnnotation();
		
		AnnotationMemory annotationMemory = new AnnotationMemory(polylineAnnotation);
					
		Integer id = addAnnotationMemory(annotationMemory);
		
		return id;
	}

	public void replayAnimationAnnotations(MainActivity delegate) {
		Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();

		while (iter.hasNext()) {
			Integer id = iter.next();
			AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);

			if (id != null && annotationMemory != null) {

				if (annotationMemory.getAnnotation() instanceof AnimationAnnotation) {
					AnimationAnnotation animationAnnotation = (AnimationAnnotation) annotationMemory.getAnnotation();

					animationAnnotation.startMediaPlayer(delegate);
				}
			}
		}
	}

	public void clearAnnotations(MainActivity delegate, boolean sendDeleteAnnotationCommands) {
		Log.d(TAG, "clearAnnotations");		
		
		Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
		
		while (iter.hasNext()) {
			Integer id = iter.next();

            if (sendDeleteAnnotationCommands) {
                NetworkUtil.sendDeleteAnnotationCommand(delegate, id);
            }

			_removeAnnotationFromMap(id);
		}
	}
	
	public Map<Integer, AnnotationMemory> getAnnotations() {
		return mAnnotationMemoryMap;
	}

	public Integer addAnnotationMemory(AnnotationMemory annotationMemory) {
		Integer id = AnnotationStates.getInstance().getNextAnnotationId();
		
		return addAnnotationMemoryWithID(id, annotationMemory);
	}
	
	public Integer addAnnotationMemoryWithID(Integer id, AnnotationMemory annotationMemory) {

		updateAnnotationMemoryWithID(id, annotationMemory);
		
		return id;
	}
	
	public void updateAnnotationMemoryWithID(Integer id, AnnotationMemory annotationMemory) {
		
		if (PeerConnection.getInstance().getOwnUserType().isTrainee() && Pref.getInstance().isShowingPreviousSteps()) {
			PreviousStepsState.getInstance().requestFrameCapture();
		}
		
		mAnnotationMemoryMap.put(id, annotationMemory);
	}
	
	public void updateAnnotationMemoryWithFrameData(AnnotationMemory annotationMemory) {
		annotationMemory.initializeMemory(annotationMemory.getAnnotation());
	}

	// this is used when we don't need all the feature data, just coordinates of points
	public JSONObject toSimpleJSON() throws JSONException {
		JSONObject annotationStateObject = new JSONObject();
		
		if (mAnnotationMemoryMap != null) {
			Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
			
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
				
				if (id != null && annotationMemory != null) {
					String idString = id.toString();
					JSONObject annotationMemoryObject = annotationMemory.toSimpleJSON();
					
					annotationStateObject.put(idString, annotationMemoryObject);
				}
			}
		}
		
		//Log.d(TAG, "result: " + annotationStateObject.toString());
		return annotationStateObject;
	}
	
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject annotationStateObject = new JSONObject();
		
		if (mAnnotationMemoryMap != null) {
			Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
			
			while (iter.hasNext()) {
				Integer id = iter.next();
				AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
				
				annotationStateObject.put(id.toString(), annotationMemory.toJSON());
			}
		}
		
		//Log.d(TAG, "result: " + annotationStateObject.toString());
		return annotationStateObject;
	}
	
	public void updateFromJSONObject(JSONObject annotationStateObject) throws JSONException {

        for (Map.Entry<Integer, AnnotationMemory> entry : mAnnotationMemoryMap.entrySet()) {
            _removeAnnotationFromMap(entry.getKey());
        }

		Iterator<?> keys = annotationStateObject.keys();
		
		while (keys.hasNext()) {
			String idString = (String)keys.next();
			
			Integer id = Integer.parseInt(idString);
			
			JSONObject annotationMemoryObject = annotationStateObject.getJSONObject(idString);
			AnnotationMemory annotationMemory = AnnotationMemory.fromJSON(annotationMemoryObject);

            mAnnotationMemoryMap.put(id, annotationMemory);
		}
	}

	public void removeAnnotationWithID(MainActivity delegate, Integer annotationId, boolean sendDeleteAnnotationCommand) {
		if (sendDeleteAnnotationCommand) {
            NetworkUtil.sendDeleteAnnotationCommand(delegate, annotationId);
        }

		_removeAnnotationFromMap(annotationId);
	}

	public void _removeAnnotationFromMap(Integer annotationId) {
		if (mAnnotationMemoryMap.containsKey(annotationId)) {
			AnnotationMemory annotationMemoryToRemove = mAnnotationMemoryMap.get(annotationId);
			if (annotationMemoryToRemove != null) {
				Annotation annotationToRemove = annotationMemoryToRemove.getAnnotation();
				if (annotationToRemove != null) {
					annotationToRemove.onDestroy();
					annotationToRemove = null;
				}
				annotationMemoryToRemove = null;
			}
			mAnnotationMemoryMap.remove(annotationId);
		}
	}

	public void translateAllAnnotations(Point point) {
		Iterator<Integer> iter = mAnnotationMemoryMap.keySet().iterator();
	
		while (iter.hasNext()) {
			Integer id = iter.next();
			AnnotationMemory annotationMemory = mAnnotationMemoryMap.get(id);
			Annotation annotation = annotationMemory.getAnnotation();

			annotation.translate(point);
		}
		
	}



	

}
