package edu.purdue.andersed.star.annotations;

import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

public class Matches {

	private List<Point> mRefFrameKeyPoints;
	private List<Point> mCurFrameKeyPoints;
	private List<Float> mDistances;
	
	public Matches() {
		super();
		this.mRefFrameKeyPoints = new ArrayList<Point>();
		this.mCurFrameKeyPoints = new ArrayList<Point>();
		this.mDistances = new ArrayList<Float>();
	}
	
	public Matches(List<Point> refFrameKeyPoints,
			List<Point> curFrameKeyPoints, List<Float> distances) {
		super();
		this.mRefFrameKeyPoints = refFrameKeyPoints;
		this.mCurFrameKeyPoints = curFrameKeyPoints;
		this.mDistances = distances;
	}
	
	public List<Point> getRefFrameKeyPoints() {
		return mRefFrameKeyPoints;
	}
	public List<Point> getCurFrameKeyPoints() {
		return mCurFrameKeyPoints;
	}
	public List<Float> getDistances() {
		return mDistances;
	}
	
}
