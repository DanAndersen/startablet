package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.Toaster;
import edu.purdue.andersed.star.settings.Pref;

public class FullAnnotationSave {

	private static final String TAG = "FullAnnotationSave";
			
	public static void saveAnnotationStateToFile() {
		long timestamp = System.currentTimeMillis();

		_saveAnnotationState(AnnotationStates.getInstance().getGlobalState(), "STAR_saved_global_annotations_" + timestamp + ".log");
		_saveAnnotationState(AnnotationStates.getInstance().getLocalState(), "STAR_saved_local_annotations_" + timestamp + ".log");
	}

	private static void _saveAnnotationState(AnnotationState annotationState, String filename) {
		STARUtils.createLogDirIfNotExist();
		String path = Pref.getInstance().getLogDir() + "/" + filename;

		Log.d(TAG, "going to create save file at " + path);

		try {
			File saveFile = new File(path);
			if (!saveFile.exists()) {
				saveFile.createNewFile();
			}

			Log.d(TAG, "setting up bufferedwriter for save");
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(saveFile, true));

			JSONObject currentFullAnnotationState = annotationState.toJSON();

			bufferedWriter.append(currentFullAnnotationState.toString());
			bufferedWriter.newLine();
			bufferedWriter.flush();

			bufferedWriter.close();

			Toaster.getInstance().toast("annotations saved! to " + path);

			bufferedWriter = null;

			saveFile = null;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
