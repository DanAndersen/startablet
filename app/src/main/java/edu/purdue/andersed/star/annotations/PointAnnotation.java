package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class PointAnnotation extends Annotation {

	private static final String TAG = "PointAnnotation";

	public PointAnnotation(MatOfPoint2f pointMat) {
		mPointData.setPointsMat(pointMat);
	}
	
	public PointAnnotation(Point pointInScreenSpace) {
        Log.d(TAG, "creating PointAnnotation");
		List<Point> pointsInScreenSpace = new ArrayList<Point>();
        pointsInScreenSpace.add(pointInScreenSpace);
		setPointsInScreenSpace(pointsInScreenSpace);
	}
	
	public PointAnnotation(double screenX, double screenY) {
		this(new Point(screenX, screenY));
	}

	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
        pointOverlay.drawPointsWithColor(mPointData, color);
	}
	
	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		drawWithColor(pointOverlay, color);
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_POINT;
	}

	@Override
	String getGeometryString() {
		Point point = getPointInScreenSpace();
		if (point != null) {
			return point.toString();	
		} else {
			return "";
		}
		
	}
	
	public Point getPointInScreenSpace() {
        List<Point> points = mPointData.getScreenSpacePoints();
        if (points.size() >= 1) {
            return points.get(0);
        } else {
            return null;
        }
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		//Log.d(TAG, "converting to JSON");
		
		JSONObject pointAnnotationObject = new JSONObject();
		
		pointAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		pointAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(mPointData.getPointsMat()));
		
		//Log.d(TAG, "result: " + pointAnnotationObject.toString());
		return pointAnnotationObject;
	}
	
	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		PointAnnotation pointAnnotation = new PointAnnotation(pointMat);
		
		return pointAnnotation;
	}

	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}

}
