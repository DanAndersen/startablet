package edu.purdue.andersed.star.ipconnect;

import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.objdetect.Objdetect;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.PeerConnection;
import edu.purdue.andersed.streaming.EncodeListener;
import edu.purdue.andersed.streaming.Encoder;

public class FrameClient implements EncodeListener {

	// used by the trainee system, establishes a connection to the mentor system and supports sending frames to the mentor server
	
	private static final String TAG = "FrameClient";

    private static FrameClient instance;



	private Thread mFrameClientLoopThread;
		
	private boolean mIsReadyToSendNewFrame;
	
	private byte[] mEncodedFrameBytesToSend;
    private int mNumEncodedBytesToSend;

    private ByteBuffer mLengthByteBuffer;

	private boolean mRunning;

    private static final Object mLockObj = new Object();

    public static FrameClient getInstance() {
        if (instance == null) {
            instance = new FrameClient();
        }
        return instance;
    }

	private FrameClient() {
		Log.d(TAG, "constructing FrameClient");
	}

    private void teardownIfNeeded() {
        Log.d(TAG, "teardownIfNeeded");
        mRunning = false;

        if (mFrameClientLoopThread != null && mFrameClientLoopThread.isAlive()) {
            Log.d(TAG, "mFrameClientLoopThread is alive, interrupting");
            mFrameClientLoopThread.interrupt();
        }

        synchronized (mLockObj) {
            mEncodedFrameBytesToSend = null;
            mNumEncodedBytesToSend = -1;
        }


    }

	public void init() {
		Log.d(TAG, "initializing FrameClient");

        teardownIfNeeded();

        mFrameClientLoopThread = new Thread(new FrameClientLoop());

        mLengthByteBuffer = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder());

		if (Pref.getInstance().isEncodingDecodingFrames()) {
			int encodeWidth = ScreenState.getInstance().getScreenWidth() / Pref.getInstance().getCvImageShrinkFactor();
			int encodeHeight = ScreenState.getInstance().getScreenHeight() / Pref.getInstance().getCvImageShrinkFactor();
			Encoder.getInstance().init(this, encodeWidth, encodeHeight);
		}

        mRunning = true;
		
		mIsReadyToSendNewFrame = true;

        mFrameClientLoopThread.start();
	}
	
	public void requestSendFrame(Mat mat) {
		if (mIsReadyToSendNewFrame) {
			
			if (Pref.getInstance().isEncodingDecodingFrames()) {
				Encoder.getInstance().requestEncodeFrame(mat);
			} else {
				//ready to send another frame, proceeding
				Mat matCopy = new Mat();
				mat.copyTo(matCopy);
				
				byte buff[] = new byte[(int) (matCopy.total() * matCopy.channels())];
				
				matCopy.get(0, 0, buff);

                synchronized (mLockObj) {
                    mEncodedFrameBytesToSend = buff;
                    mNumEncodedBytesToSend = buff.length;
                }
			}
			
			mIsReadyToSendNewFrame = false;
		} else {
			// system isn't ready to send another frame, skipping this one
		}
	}
	
	
	public void destroy() {
		Log.d(TAG, "destroying FrameClient");
        teardownIfNeeded();

        Encoder.getInstance().destroy();
	}



    private class FrameClientLoop implements Runnable {
        private static final String TAG = "FrameClientLoop";

        private Socket mSocket;
        private OutputStream mOut;

        public void run() {
            try {
                while (mRunning && !Thread.currentThread().isInterrupted()) {
                    try {

                        String host = PeerConnection.getInstance().getMentorIP();
                        int port = Pref.getInstance().getVideoServerSocketPort();

                        Log.d(TAG, "going to connect to " + host + ":" + port);

                        mSocket = new Socket(host, port);

                        Log.d(TAG, "started socket");

                        mOut = mSocket.getOutputStream();

                        while (mRunning && !Thread.currentThread().isInterrupted() && mSocket.isConnected()) {
                            while (mRunning && mSocket.isConnected()) {
                                synchronized (mLockObj) {
                                    if (mEncodedFrameBytesToSend != null && mNumEncodedBytesToSend > 0) {
                                        mIsReadyToSendNewFrame = false;

                                        mLengthByteBuffer.rewind();
                                        byte[] lengthBytes = mLengthByteBuffer.putInt(mNumEncodedBytesToSend).array();

                                        //Log.d(TAG, "sending out " + mNumEncodedBytesToSend + " encoded bytes");

                                        if (Pref.getInstance().isEncodingDecodingFrames()) {
                                            mOut.write(lengthBytes);
                                        }
                                        mOut.write(mEncodedFrameBytesToSend, 0, mNumEncodedBytesToSend);
                                        mOut.flush();

                                        mEncodedFrameBytesToSend = null;
                                        mNumEncodedBytesToSend = -1;
                                    }
                                }

                                mIsReadyToSendNewFrame = true;
                            }
                        }

                    } catch (ConnectException e) {
                        Log.d(TAG, e.getMessage());
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    int retryMillis = Pref.getInstance().getClientRetryMillis();
                    Log.d(TAG, "data client connection closed. waiting for " + retryMillis + " ms to retry.");
                    Thread.sleep(retryMillis);
                }
            } catch (InterruptedException e) {
                Log.d(TAG, "Interrupted: " + e.getMessage());
            } finally {
                if (mOut != null) {
                    try {
                        Log.d(TAG, "finishing up frameclient, closing open outputstream");
                        mOut.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (mSocket != null) {
                    try {
                        Log.d(TAG, "finishing up frameclient, closing open socket");
                        mSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mSocket = null;
                }
            }
            Log.d(TAG, "ending run()");
        }
    }

	@Override
	public void onFrameEncoded(byte[] encodedFrameBufferBytes, int numBytesInEncodedFrame) {
        if (mIsReadyToSendNewFrame) {
            mEncodedFrameBytesToSend = encodedFrameBufferBytes;
            mNumEncodedBytesToSend = numBytesInEncodedFrame;
        } else {
            Log.d(TAG, "onFrameEncoded, not sending this encoded frame because the client is busy sending a frame already");
        }
	}
	
}
