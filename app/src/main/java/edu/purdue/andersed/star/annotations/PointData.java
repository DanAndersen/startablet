package edu.purdue.andersed.star.annotations;

import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.geometry.Line;
import edu.purdue.andersed.star.geometry.NDCGeometry;

/**
 * Created by andersed on 10/9/2016.
 */

/*
To avoid constantly recreating/converting data about annotation points (e.g. points for line segments),
this helper class contains several representations of the same data, all kept coherent:
- a MatOfPoint2f ("opencv format") used when communicating between devices.
- a List<Point> in screen space coordinates
- an NDCGeometry used for rendering to screen directly.
 */
public class PointData {
    private MatOfPoint2f mPointMat;
    private CopyOnWriteArrayList<Point> mPointsInScreenSpace;
    private NDCGeometry mNDCGeometry;

    private int mScreenWidth;
    private int mScreenHeight;

    public PointData() {
        reset();
    }

    public List<Point> getScreenSpacePoints(){
        synchronized (this) {
            return mPointsInScreenSpace;
        }
    }

    public MatOfPoint2f getPointsMat() {
        synchronized (this) {
            return mPointMat;
        }
    }

    public NDCGeometry getNDCGeometry() {
        synchronized (this) {
            return mNDCGeometry;
        }
    }

    public void reset() {
        synchronized (this) {
            mScreenWidth = ScreenState.getInstance().getScreenWidth();
            mScreenHeight = ScreenState.getInstance().getScreenHeight();

            mPointMat = new MatOfPoint2f();
            mPointsInScreenSpace = new CopyOnWriteArrayList<Point>();
            mNDCGeometry = new NDCGeometry();
        }
    }

    private void updateNDCGeometry() {
        synchronized (this) {
            float[] verts = new float[mPointsInScreenSpace.size() * 3];
            int idx = 0;
            for (Point screenSpacePoint : mPointsInScreenSpace) {
                verts[idx + 0] = screenXToNDCX(screenSpacePoint.x);
                verts[idx + 1] = screenYToNDCY(screenSpacePoint.y);
                verts[idx + 2] = NDCGeometry.MAX_Z;
                idx += 3;
            }
            mNDCGeometry.setVerts(verts);
        }
    }

    private float screenXToNDCX(double screenX) {
        return (float)((2.0f/mScreenWidth) * screenX - 1);
    }

    private float screenYToNDCY(double screenY) {
        return (float)(1 - (2.0f/mScreenHeight) * screenY);
    }

    public void addScreenSpacePoint(Point pointInScreenSpace) {
        synchronized (this) {
            Point pointInOpenCVSpace = ScreenState.getInstance().screenSpaceToOpenCVSpace(pointInScreenSpace);
            mPointMat.push_back(new MatOfPoint2f(pointInOpenCVSpace));
            mPointsInScreenSpace.add(pointInScreenSpace);
            updateNDCGeometry();
        }
    }

    public void setPointsMat(MatOfPoint2f pointMat) {
        synchronized (this) {
            mPointMat = pointMat;
            mPointsInScreenSpace = new CopyOnWriteArrayList<Point>();

            List<Point> pointsInOpenCVSpace = pointMat.toList();
            for (Point pointInOpenCVSpace : pointsInOpenCVSpace) {
                Point pointInScreenSpace = ScreenState.getInstance().openCVSpaceToScreenSpace(pointInOpenCVSpace);
                mPointsInScreenSpace.add(pointInScreenSpace);
            }

            updateNDCGeometry();
        }
    }
}
