package edu.purdue.andersed.star.annotations;

/**
 * Created by andersed on 1/17/2017.
 */

public class AnnotationStates {
    private static AnnotationStates instance;
    private AnnotationState mGlobalAnnotationState; // the state of all annotations that should be common between mentor and trainee
    private AnnotationState mLocalAnnotationState;  // the state of any annotations that should not be propagated beyond the local device

    private Integer mAnnotationIdIndex;	// every annotation memory has a particular integer ID -- increments on creation, does not decrement on deletion; goal is to have each annotation with a unique id per session

    public static AnnotationStates getInstance() {
        if (instance == null) {
            instance = new AnnotationStates();
        }
        return instance;
    }

    private AnnotationStates() {
        mGlobalAnnotationState = new AnnotationState();
        mLocalAnnotationState = new AnnotationState();

        mAnnotationIdIndex = 0;
    }

    public synchronized Integer getNextAnnotationId() {
        mAnnotationIdIndex++;
        return mAnnotationIdIndex;
    }

    public void onPause() {
        mGlobalAnnotationState.onPause();
        mLocalAnnotationState.onPause();
    }

    public AnnotationState getGlobalState() {
        return mGlobalAnnotationState;
    }

    public AnnotationState getLocalState() {
        return mLocalAnnotationState;
    }
}
