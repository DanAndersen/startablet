package edu.purdue.andersed.star.annotations;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.Surface;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.io.IOException;
import java.util.List;

import edu.purdue.andersed.star.shaders.ToolTextureStorage;
import edu.purdue.andersed.star.wifidirect.Protocol;

/**
 * Created by andersed on 11/4/2016.
 */

public class AnimationAnnotation extends ToolAnnotation {

    private static final String TAG = "AnimationAnnotation";

    private boolean mMediaPlayerInitialized;
    private MediaPlayer mMediaPlayer;

    private int mVideoStageIndex;
    private String mCurrentVideoStageAssetFilename;

    private boolean mMediaPlayerIsPrepared;

    private boolean mIsActive; // false if the surface is detached from the context

    public AnimationAnnotation(Point pointInScreenSpace, ToolType toolType, double degrees, double scale, int selectableColor, int videoStageIndex) {
        super(pointInScreenSpace, toolType, degrees, scale, selectableColor);

        updateVideoStage(videoStageIndex);

        mIsActive = true;
    }

    public AnimationAnnotation(MatOfPoint2f pointMat, ToolType toolType, double degrees, double scale, int selectableColor, int videoStageIndex) {
        super(pointMat, toolType, degrees, scale, selectableColor);

        updateVideoStage(videoStageIndex);

        mIsActive = true;
    }

    public void updateVideoStage(int newVideoStageIndex) {
        mVideoStageIndex = newVideoStageIndex;
        mCurrentVideoStageAssetFilename = "";
        mMediaPlayerInitialized = false;
    }

    public boolean isMediaPlayerInitialized() {
        return mMediaPlayerInitialized;
    }

    @Override
    public void onPause() {
        teardownMediaPlayer();
    }

    private void teardownMediaPlayer() {
        synchronized (this) {
            Log.d(TAG, "begin onDestroy");
            if (mMediaPlayer != null) {
                Log.d(TAG, "stopping and resetting mMediaPlayer");
                if (mMediaPlayerIsPrepared) {
                    mMediaPlayer.stop();
                    mMediaPlayerIsPrepared = false;
                }
                mMediaPlayer.setSurface(null);
                mMediaPlayer.reset();
                mMediaPlayer = null;
            }
            mMediaPlayerInitialized = false;
            mIsActive = false;
            Log.d(TAG, "end onDestroy");
        }
    }

    @Override
    public void onDestroy() {
        teardownMediaPlayer();
    }

    public void startMediaPlayer(Context context) {
        synchronized (this) {
            Log.d(TAG, "begin startMediaPlayer");

            AnimationTemplate animationTemplate = (AnimationTemplate) mToolType.getToolTemplate();
            SurfaceTexture surfaceTexture = ToolTextureStorage.getInstance().getSurfaceTexture(mToolType);
            if (surfaceTexture == null) {
                Log.e(TAG, "attempted to start media player for this animation annotation, but the surface texture is null. do nothing.");
                return;
            }


            if (mMediaPlayer != null) {
                Log.d(TAG, "mMediaPlayer already exists, stopping and resetting");
                if (mMediaPlayerIsPrepared) {
                    mMediaPlayer.stop();
                    mMediaPlayerIsPrepared = false;
                }
                mMediaPlayer.setSurface(null);
                mMediaPlayer.reset();
            } else {
                Log.d(TAG, "initing mMediaPlayer");
                mMediaPlayer = new MediaPlayer();
            }



            String videoAssetFilename;
            if (mVideoStageIndex >= 0 && mVideoStageIndex < animationTemplate.getVideoStageAssetFilenames().size()) {
                videoAssetFilename = animationTemplate.getVideoStageAssetFilenames().get(mVideoStageIndex);
            } else {
                videoAssetFilename = animationTemplate.getVideoStageAssetFilenames().get(0);
            }

            try {
                AssetFileDescriptor afd = context.getAssets().openFd(videoAssetFilename);
                mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mCurrentVideoStageAssetFilename = videoAssetFilename;


                mMediaPlayer.setSurface(new Surface(surfaceTexture));

                mMediaPlayer.setLooping(false);

                Log.d(TAG, "preparing mMediaPlayer");
                mMediaPlayer.prepare();
                mMediaPlayerIsPrepared = true;

                Log.d(TAG, "starting mMediaPlayer");
                mMediaPlayer.start();

                mMediaPlayerInitialized = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            mIsActive = true;
            Log.d(TAG, "end startMediaPlayer");
        }
    }

    public boolean isPlayingCorrectVideo() {
        synchronized (this) {
            //Log.d(TAG, "isPlayingCorrectVideo");
            //Log.d(TAG, "mVideoStageIndex: " + mVideoStageIndex);
            AnimationTemplate animationTemplate = (AnimationTemplate) mToolType.getToolTemplate();
            if (mVideoStageIndex < 0 || mVideoStageIndex >= animationTemplate.getVideoStageAssetFilenames().size()) {
                return false;
            } else {
                String correctVideoAssetFilename = animationTemplate.getVideoStageAssetFilenames().get(mVideoStageIndex);
                //Log.d(TAG, "correctVideoAssetFilename: " + correctVideoAssetFilename);
                //Log.d(TAG, "mCurrentVideoStageAssetFilename: " + mCurrentVideoStageAssetFilename);
                return (correctVideoAssetFilename.equals(mCurrentVideoStageAssetFilename));
            }
        }
    }

    public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
        MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));

        ToolType toolType = ToolType.fromTag(annotationJSONObject.getString(TAG_ANNOTATION_TOOL_TYPE));

        double degrees = annotationJSONObject.getDouble(TAG_ANNOTATION_ROTATION);
        double scale = annotationJSONObject.getDouble(TAG_ANNOTATION_SCALE);
        int selectableColor = annotationJSONObject.getInt(TAG_ANNOTATION_SELECTABLE_COLOR);

        String videoStage = annotationJSONObject.getString(TAG_ANNOTATION_VIDEO_STAGE);

        AnimationTemplate animationTemplate = (AnimationTemplate) toolType.getToolTemplate();
        List<String> videoStageLabels = animationTemplate.getVideoStageLabels();

        int videoStageIndex = -1;
        for (int i = 0; i < videoStageLabels.size(); i++) {
            String label = videoStageLabels.get(i);
            if (label.toLowerCase().equals(videoStage.toLowerCase())) {
                videoStageIndex = i;
                break;
            }
        }

        if (videoStageIndex == -1) {
            Log.e(TAG, "ERROR: when loading animation annotation, was unable to find the correct video stage index given label '" + videoStage + "'.");
            videoStageIndex = 0;
        }

        AnimationAnnotation animationAnnotation = new AnimationAnnotation(pointMat, toolType, degrees, scale, selectableColor, videoStageIndex);

        return animationAnnotation;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject annotationObject = new JSONObject();

        annotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
        annotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(mPointData.getPointsMat()));
        annotationObject.put(TAG_ANNOTATION_TOOL_TYPE, getToolType().getTag());
        annotationObject.put(TAG_ANNOTATION_ROTATION, mDegrees);
        annotationObject.put(TAG_ANNOTATION_SCALE, mScale);
        annotationObject.put(TAG_ANNOTATION_SELECTABLE_COLOR, mSelectableColor);

        AnimationTemplate animationTemplate = (AnimationTemplate) getToolType().getToolTemplate();
        String videoStageLabel = animationTemplate.getVideoStageLabels().get(mVideoStageIndex);

        annotationObject.put(TAG_ANNOTATION_VIDEO_STAGE, videoStageLabel);

        return annotationObject;
    }

    @Override
    String getAnnotationType() {
        return Annotation.TYPE_ANIMATION;
    }

    public boolean isActive() {
        return mIsActive;
    }
}
