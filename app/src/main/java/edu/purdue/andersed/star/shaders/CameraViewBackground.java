package edu.purdue.andersed.star.shaders;

import javax.microedition.khronos.opengles.GL10;

public interface CameraViewBackground {

    void draw(GL10 gl);
}
