package edu.purdue.andersed.star.annotations;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import java.io.IOException;
import java.io.InputStream;

public class ToolTemplate {

	// how far right/down from the image center the anchor point is
	private int mAnchorPixelsFromCenterX;
	private int mAnchorPixelsFromCenterY;

    private Drawable mDrawable;

    // can be loaded from a drawable resource id...
    // or from an image in the assets directory
    private enum DrawableSource {
        RESOURCE_ID, ASSET_LABEL
    };

    private final DrawableSource mDrawableSource;
    private int mDrawableResourceId;
    private String mAssetFilename;

    public Class<?> getAnnotationClass() {
        return ToolAnnotation.class;
    }

	public ToolTemplate(int drawableResourceId, int imageWidth, int imageHeight, int anchorX, int anchorY) {
        mDrawableSource = DrawableSource.RESOURCE_ID;
        mDrawableResourceId = drawableResourceId;

        this.mAnchorPixelsFromCenterX = anchorX - (imageWidth/2);
		this.mAnchorPixelsFromCenterY = anchorY - (imageHeight/2);
	}

    public ToolTemplate(String assetFilename, int imageWidth, int imageHeight, int anchorX, int anchorY) {
        mDrawableSource = DrawableSource.ASSET_LABEL;
        mAssetFilename = assetFilename;


        this.mAnchorPixelsFromCenterX = anchorX - (imageWidth/2);
        this.mAnchorPixelsFromCenterY = anchorY - (imageHeight/2);
    }

	public Drawable getDrawable(Context context) {
        if (mDrawable == null) {
            if (mDrawableSource == DrawableSource.RESOURCE_ID) {
                mDrawable = ContextCompat.getDrawable(context, mDrawableResourceId);
            } else if (mDrawableSource == DrawableSource.ASSET_LABEL) {
                try {
                    InputStream is = context.getAssets().open(mAssetFilename);
                    mDrawable = Drawable.createFromStream(is, null);
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return mDrawable;
    }

	public int getAnchorPixelsFromCenterX() {
		return mAnchorPixelsFromCenterX;
	}

	public int getAnchorPixelsFromCenterY() {
		return mAnchorPixelsFromCenterY;
	}
	
	
	
	
}
