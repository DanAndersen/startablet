package edu.purdue.andersed.star.shaders;

import android.graphics.Color;
import android.opengl.GLES30;
import android.util.Log;

import org.opencv.core.Point;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.annotations.PointData;
import edu.purdue.andersed.star.geometry.Line;
import edu.purdue.andersed.star.geometry.NDCGeometry;
import edu.purdue.andersed.star.settings.Pref;

public class LineShaderProgram {

    private static final String TAG = "LineShaderProgram";

    private static final String LINE_VERTEX_SHADER =
            "attribute vec4 vertexPosition; \n" +
                    "void main() \n" +
                    "{ \n" +
                    "    gl_Position = vertexPosition; \n" +
                    "} \n"
            ;

    private static final String LINE_FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float; \n" +
                    "uniform vec4 lineColor; \n" +
                    "void main() \n" +
                    "{ \n" +
                    "    gl_FragColor = lineColor; \n" +
                    "} \n"
            ;

    private int mLineShaderProgramID;

    private int mLineVertexHandle;

    private int mLineColorHandle;

    public LineShaderProgram() {
        setupLineShaderProgram();
    }

    private void setupLineShaderProgram() {
        Log.d(TAG, "creating shader program");
        mLineShaderProgramID = STARUtils.createProgramFromShaderSrc(LINE_VERTEX_SHADER, LINE_FRAGMENT_SHADER );
        STARUtils.checkGLError("createProgramFromShaderSrc");

        Log.d(TAG, "setting up handles");
        mLineVertexHandle = GLES30.glGetAttribLocation(mLineShaderProgramID, "vertexPosition");
        STARUtils.checkGLError("glGetAttribLocation");

        mLineColorHandle = GLES30.glGetUniformLocation(mLineShaderProgramID, "lineColor");
        STARUtils.checkGLError("glGetUniformLocation");
    }

    // takes input in screen coords, will convert to NDC
    public void drawLineWithColor(Point p1, Point p2, int color) {
        this.drawLineWithColorAndThickness(p1, p2, color, Pref.getInstance().getDefaultLineThickness());
    }

    public void drawLineWithColorAndThickness(Point p1, Point p2, int color, float thickness) {

    }

    public void drawLinesWithColorAndThickness(PointData pointData, int color, float thickness, boolean closed, int numVerticesToDraw) {

        NDCGeometry lineGeometry = pointData.getNDCGeometry();

        GLES30.glLineWidth(thickness+2);

        GLES30.glUseProgram(mLineShaderProgramID);
        STARUtils.checkGLError("glUseProgram");

        GLES30.glVertexAttribPointer(mLineVertexHandle, 3, GLES30.GL_FLOAT, false, 0, lineGeometry.getVertices());
        STARUtils.checkGLError("glVertexAttribPointer");

        GLES30.glEnableVertexAttribArray(mLineVertexHandle);
        STARUtils.checkGLError("glEnableVertexAttribArray");

        GLES30.glUniform4f(mLineColorHandle, 0, 0, 0, 255);
        STARUtils.checkGLError("glUniform4f");

        int mode = (closed ? GLES30.GL_LINE_LOOP : GLES30.GL_LINE_STRIP);

        int numVertices = lineGeometry.getNumVertices();
        if (numVerticesToDraw >= 0) {
            numVertices = numVerticesToDraw;
        }

        GLES30.glDrawArrays(mode, 0, numVertices);
        STARUtils.checkGLError("glDrawArrays");

        // now drawing the main line
        GLES30.glLineWidth(thickness);

        GLES30.glUniform4f(mLineColorHandle, (float)Color.red(color) / 255, (float)Color.green(color) / 255, (float)Color.blue(color) / 255, 255);
        STARUtils.checkGLError("glUniform4f");

        GLES30.glDrawArrays(mode, 0, lineGeometry.getNumVertices());
        STARUtils.checkGLError("glDrawArrays");

        GLES30.glDisableVertexAttribArray(mLineVertexHandle);
    }
}
