package edu.purdue.andersed.star.ipconnect;

import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import edu.purdue.andersed.star.STARUtils;
import edu.purdue.andersed.star.commands.Command;
import edu.purdue.andersed.star.settings.Pref;

public class DataServer {

    // used by the mentor system, receives an incoming connection from a trainee system and supports sending to the trainee client

    private static final String TAG = "DataServer";

    private Thread mDataServerLoopThread;

    private boolean mRunning;

    private ServerSocket mServerSocket;

    private Queue<Command> mCommandsToSend;

    public DataServer() {
        Log.d(TAG, "constructing DataServer");

        mCommandsToSend = new ConcurrentLinkedQueue<Command>();
    }

    private void teardownIfNeeded() {
        Log.d(TAG, "teardownIfNeeded");
        mRunning = false;

        if (mServerSocket != null) {
            try {
                Log.d(TAG, "trying to close server socket");
                mServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mServerSocket = null;
        }

        if (mDataServerLoopThread != null && mDataServerLoopThread.isAlive()) {
            Log.d(TAG, "interrupting thread");
            mDataServerLoopThread.interrupt();
        }
    }

    public void init() {
        Log.d(TAG, "initializing DataServer");

        teardownIfNeeded();

        mDataServerLoopThread = new Thread(new DataServerLoop());

        mRunning = true;

        mDataServerLoopThread.start();
    }

    public void requestSendCommand(Command command) {
        Log.d(TAG, "requestSendCommand");
        mCommandsToSend.add(command);
    }

    public void destroy() {
        Log.d(TAG, "destroying DataServer");
        teardownIfNeeded();
    }

    private class DataServerLoop implements Runnable {
        private static final String TAG = "DataServerLoop";

        public void run() {
            Log.d(TAG, "starting run()");

            try {
                while (mRunning && !Thread.currentThread().isInterrupted()) {
                    try {
                        int port = Pref.getInstance().getDataServerSocketPort();
                        Log.d(TAG, "starting server on port " + port);

                        mServerSocket = new ServerSocket();
                        mServerSocket.setReuseAddress(true);
                        mServerSocket.bind(new InetSocketAddress(port));
                        Log.d(TAG, "started server socket, waiting for connection...");

                        while (mRunning && !Thread.currentThread().isInterrupted() && !mServerSocket.isClosed()) {
                            Log.d(TAG, "waiting for connection...");
                            Socket clientSocket = mServerSocket.accept();
                            Log.d(TAG, "got client connection");

                            OutputStreamWriter out = new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8);

                            while (mRunning && clientSocket.isConnected()) {

                                while (mRunning && !mCommandsToSend.isEmpty()) {
                                    try {
                                        Command command = mCommandsToSend.remove();

                                        Log.d(TAG, "writing data...");

                                        String stringToWrite = command.toJSON().toString() + "\n";

                                        out.write(stringToWrite);
                                        out.flush();

                                        STARUtils.saveStringToFile(stringToWrite, "star_sent_command_" + System.currentTimeMillis() + ".log");

                                        Log.d(TAG, "data written");
                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    int retryMillis = Pref.getInstance().getClientRetryMillis();
                    Log.d(TAG, "data server shut down. waiting for " + retryMillis + " ms to retry.");
                    Thread.sleep(retryMillis);
                }
            } catch (InterruptedException e) {
                Log.d(TAG, "Interrupted: " + e.getMessage());
            } finally {
                Log.d(TAG, "closing server socket if needed");
                if (mServerSocket != null) {
                    try {
                        mServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mServerSocket = null;
                }
            }
            Log.d(TAG, "ending run()");
        }
    }
}