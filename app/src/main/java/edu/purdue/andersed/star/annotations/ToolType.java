package edu.purdue.andersed.star.annotations;

import java.util.Arrays;

import edu.purdue.andersed.star.R;

public enum ToolType {
	BVM("bvm", new ToolTemplate(
			R.drawable.bvm_shadow, 
			572, 268,
			63, 119)),
	ETTUBE("ettube", new ToolTemplate(
			R.drawable.ettube_shadow, 
			650, 328,
			42, 168)),
	HEMOSTAT("hemostat", new ToolTemplate(
			R.drawable.hemostat_shadow, 
			495, 254,
			6, 125)),
	IODINESWAB("iodineswab", new ToolTemplate(
			R.drawable.iodineswab_shadow, 
			775, 96,
			44, 47)),
	RETRACTOR("retractor", new ToolTemplate(
			R.drawable.retractor_shadow, 
			460, 358,
			47, 164)),
	SCALPEL("scalpel", new ToolTemplate(
			R.drawable.scalpel_shadow, 
			475, 51,
			9, 25)),
	STETHOSCOPE("stethoscope", new ToolTemplate(
			R.drawable.stethoscope_shadow, 
			560, 580,
			92, 320)),
	SURGICALTAPE("surgicaltape", new ToolTemplate(
			R.drawable.surgicaltape_shadow, 
			394, 348,
			51, 294)),
	SYRINGE("syringe", new ToolTemplate(
			R.drawable.syringe_shadow, 
			534, 103,
			9, 55)),
	TWEEZERS("tweezers", new ToolTemplate(
			R.drawable.tweezers_shadow, 
			558, 90,
			21, 43)),
	LONGHOOK("longhook", new ToolTemplate(
			R.drawable.longhook_shadow, 
			592, 48,
			33, 24)),
	SCISSORS("scissors", new ToolTemplate(
			R.drawable.scissors_shadow, 
			336, 198,
			15, 96)),
			
	TEXT_CLOSE("text_close", new ToolTemplate(
			R.drawable.text_close_shadow, 
			211, 66,
			105, 31)),
	TEXT_INCISION("text_incision", new ToolTemplate(
			R.drawable.text_incision_shadow, 
			309, 66,
			155, 32)),
	TEXT_PALPATION("text_palpation", new ToolTemplate(
			R.drawable.text_palpation_shadow,
			377, 79,
			377/2, 79/2)),
	TEXT_REMOVE("text_remove", new ToolTemplate(
			R.drawable.text_remove_shadow, 
			305, 51,
			305/2, 51/2)),
	TEXT_STITCH("text_stitch", new ToolTemplate(
			R.drawable.text_stitch_shadow, 
			228, 66,
			228/2, 66/2)),
			
	HAND_PALPATE("hand_palpate", new ToolTemplate(
			R.drawable.hand_palpate_shadow, 
			290, 141,
			16, 19)),
			
	HAND_POINT("hand_point", new ToolTemplate(
			R.drawable.hand_point_shadow, 
			287, 157,
			12, 19)),
			
	HAND_STRETCH("hand_stretch", new ToolTemplate(
			R.drawable.hand_stretch_shadow, 
			256, 237,
			36, 120)),

	/*
	VIDEO_CRIC("cric", new AnimationTemplate(
			"cric/cric_preview_image.jpg",
			414, 270,
			0, 0, Arrays.asList("cric_stage1", "cric_stage2", "cric_stage3", "cric_stage4", "cric_stage5", "cric_stage6"),
			Arrays.asList("cric/cric_stage1.mp4", "cric/cric_stage2.mp4", "cric/cric_stage3.mp4", "cric/cric_stage4.mp4", "cric/cric_stage5.mp4", "cric/cric_stage6.mp4")));
	*/

	VIDEO_CRIC("cric", new AnimationTemplate(
			"cric_v3/cric_v3_image.jpg",
			   320, 180,
			320/4, 180/4,
	Arrays.asList(
			"cric_stage1",
			"cric_stage2",
			"cric_stage3",
			"cric_stage4",
			"cric_stage5",
			"cric_stage6",
			"cric_stage7"),
	Arrays.asList(
			"Locate cricoid cartilage",
			"Make incision",
			"Expose cartilage",
			"Incision on cartilage",
			"Insert endotracheal tube",
			"Connect air pump",
			"Secure dressing"),
	Arrays.asList(
			"cric_v3/new_step1_out_processed.mp4",
			"cric_v3/new_step2_out_processed.mp4",
			"cric_v3/new_step3_out_processed.mp4",
			"cric_v3/new_step4_out_processed.mp4",
			"cric_v3/new_step5_out_processed.mp4",
			"cric_v3/new_step6_out_processed.mp4",
			"cric_v3/new_step7_out_processed.mp4"))),

	VIDEO_CRIC_TRIMMED("cric_trimmed", new AnimationTemplate(
			"cric_v3_trimmed/cric_v3_trimmed_image.jpg",
			208, 180,
					   208/4, 180/4,
			   Arrays.asList(
					   "cric_stage1",
			   "cric_stage2",
					   "cric_stage3",
					   "cric_stage4",
					   "cric_stage5",
					   "cric_stage6",
					   "cric_stage7"),
	Arrays.asList(
			"Locate cricoid cartilage",
			"Make incision",
			"Expose cartilage",
			"Incision on cartilage",
			"Insert endotracheal tube",
			"Connect air pump",
			"Secure dressing"),
			Arrays.asList(
			"cric_v3_trimmed/new_step1_out_trimmed_processed.mp4",
			"cric_v3_trimmed/new_step2_out_trimmed_processed.mp4",
			"cric_v3_trimmed/new_step3_out_trimmed_processed.mp4",
			"cric_v3_trimmed/new_step4_out_trimmed_processed.mp4",
			"cric_v3_trimmed/new_step5_out_trimmed_processed.mp4",
			"cric_v3_trimmed/new_step6_out_trimmed_processed.mp4",
			"cric_v3_trimmed/new_step7_out_trimmed_processed.mp4")));

	//--------------------------------
	
	private String mTag;
	private ToolTemplate mToolTemplate;
	
	ToolType(String tag, ToolTemplate toolTemplate) {
		this.mTag = tag;
		this.mToolTemplate = toolTemplate;
	}
	
	public String getTag() {
		return mTag;
	}
	
	public ToolTemplate getToolTemplate() {
		return mToolTemplate;
	}
	
	public static ToolType fromTag(String tag) {
		if (tag != null) {
			for (ToolType tt : ToolType.values()) {
				if (tag.equalsIgnoreCase(tt.getTag())) {
					return tt;
				}
			}
		}
		
		throw new IllegalStateException("unknown tool type: " + tag);
		
	}
}