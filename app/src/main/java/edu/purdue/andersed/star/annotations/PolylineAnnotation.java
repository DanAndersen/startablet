package edu.purdue.andersed.star.annotations;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import edu.purdue.andersed.star.PointOverlay;
import edu.purdue.andersed.star.ScreenState;
import edu.purdue.andersed.star.settings.Pref;
import edu.purdue.andersed.star.wifidirect.Protocol;

public class PolylineAnnotation extends Annotation {

	private static final String TAG = "PolylineAnnotation";

	public PolylineAnnotation(MatOfPoint2f pointMat) {
		mPointData.setPointsMat(pointMat);
	}
	
	public PolylineAnnotation() {
		Log.d(TAG, "creating empty PolylineAnnotation");
	}

	@Override
	public void drawWithColor(PointOverlay pointOverlay, int color) {
		drawWithColorAndThickness(pointOverlay, color, Pref.getInstance().getDefaultLineThickness());
	}

	@Override
	public void drawWithColorAndThickness(PointOverlay pointOverlay, int color,
			float thickness) {
		pointOverlay.drawLinesWithColorAndThickness(mPointData, color, thickness, false, -1);
	}

	@Override
	String getAnnotationType() {
		return Annotation.TYPE_POLYLINE;
	}

	@Override
	String getGeometryString() {
		List<Point> pointsInScreenSpace = getPointsInScreenSpace();
		
		return pointsInScreenSpace.toString();
	}

	@Override
	public JSONObject toJSON() throws JSONException {
		
		JSONObject polylineAnnotationObject = new JSONObject();
		
		polylineAnnotationObject.put(TAG_ANNOTATION_TYPE, getAnnotationType());
		polylineAnnotationObject.put(TAG_ANNOTATION_POINTS, Protocol.matOfPoint2fToJSONArray(mPointData.getPointsMat()));
		
		return polylineAnnotationObject;
	}

	public static Annotation fromJSON(JSONObject annotationJSONObject) throws JSONException {
		MatOfPoint2f pointMat = Protocol.jsonArrayToMatOfPoint2f(annotationJSONObject.getJSONArray(TAG_ANNOTATION_POINTS));
		
		PolylineAnnotation polylineAnnotation = new PolylineAnnotation(pointMat);
		
		return polylineAnnotation;
	}

	@Override
	public void translate(Point point) {
		List<Point> pointsInScreenSpace = this.getPointsInScreenSpace();
		for (Point p : pointsInScreenSpace) {
			p.x += point.x;
			p.y += point.y;
		}
		this.setPointsInScreenSpace(pointsInScreenSpace);
	}

}
