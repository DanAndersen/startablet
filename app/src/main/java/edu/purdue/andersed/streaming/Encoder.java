package edu.purdue.andersed.streaming;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.Objdetect;

public class Encoder {

    private static final String TAG = "Encoder";

    private int mEncodeWidth;
    private int mEncodeHeight;
    private Size mEncodeSize;

    private boolean mCurrentlyEncodingFrame;

    private Mat mRGBMatToEncode;

    private EncodeListener mListener;

    private static Encoder instance = new Encoder();

    private byte[] mEncodedFrameBuffer = new byte[500000];

    private boolean mIsLoaded;

    private Thread mEncoderLoopThread;
    private boolean mRunning;

    private static final Object mSyncObj = new Object();

    static
    {
        try
        {
            Log.d(TAG, "loading hello-android-jni");
            System.loadLibrary("hello-android-jni");
            Log.d(TAG, "hello-android-jni has been loaded");
        }
        catch( UnsatisfiedLinkError e )
        {
            System.err.println("Native code library failed to load.\n" + e);
            throw new RuntimeException("unable to load JNI module.");
        }
    }

    public static Encoder getInstance() {
        if (instance == null) {
            instance = new Encoder();
        }
        return instance;
    }

    public void requestEncodeFrame(Mat frameToEncode) {
        //Log.d(TAG, "requesting encode of frame...");
        if (!mCurrentlyEncodingFrame) {
            Imgproc.resize(frameToEncode, mRGBMatToEncode, mEncodeSize);
            mCurrentlyEncodingFrame = true;
        } else {
            Log.d(TAG, "skipping encoding of this frame -- currently encoding an existing frame");
        }
    }

    private Encoder() {
        Log.d(TAG, "initializing Encoder");
    }

    private void teardownIfNeeded() {
        mRunning = false;

        if (mEncoderLoopThread != null && mEncoderLoopThread.isAlive()) {
            Log.d(TAG, "interrupting thread");
            mEncoderLoopThread.interrupt();
        }
    }

    public void init(EncodeListener listener, int encodeWidth, int encodeHeight) {
        Log.d(TAG, "init");

        teardownIfNeeded();

        mEncoderLoopThread = new Thread(new EncoderLoop());

        mCurrentlyEncodingFrame = false;

        mListener = listener;
        mEncodeWidth = encodeWidth;
        mEncodeHeight = encodeHeight;
        mEncodeSize = new Size(mEncodeWidth, mEncodeHeight);

        synchronized (mSyncObj) {
            mIsLoaded = nInitEncoder(mEncodeWidth, mEncodeHeight);
        }

        mRGBMatToEncode = new Mat(mEncodeHeight, mEncodeWidth, CvType.CV_8UC3);

        mRunning = true;

        mEncoderLoopThread.start();
    }

    private class EncoderLoop implements Runnable {
        private static final String TAG = "EncoderLoop";

        public void run() {
            Log.d(TAG, "starting run()");
            while (mRunning && !Thread.currentThread().isInterrupted()) {
                if (mCurrentlyEncodingFrame) {
                    Mat matToEncode = mRGBMatToEncode;

                    //Log.d(TAG, "starting nEncode...");
                    long start = System.currentTimeMillis();
                    int numBytesInEncodedFrame;
                    try {
                        synchronized (mSyncObj) {
                            numBytesInEncodedFrame = nEncode(matToEncode.getNativeObjAddr(), mEncodedFrameBuffer);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "error in encoding: " + e.getMessage());
                        e.printStackTrace();
                        numBytesInEncodedFrame = -1;
                    }

                    long end = System.currentTimeMillis();
                    //Log.d(TAG, "did nEncode, took " + (end-start) + " millis, encodedBytes size = " + numBytesInEncodedFrame);

                    if (numBytesInEncodedFrame > 0) {
                        if (mListener != null) {
                            mListener.onFrameEncoded(mEncodedFrameBuffer, numBytesInEncodedFrame);
                        }
                    } else {
                        Log.e(TAG, "encoded bytes were null");
                    }
                    mCurrentlyEncodingFrame = false;
                }
            }
            Log.d(TAG, "ending run()");
        }
    }

    public void destroy() {
        Log.d(TAG, "destroy");

        teardownIfNeeded();

        if (mIsLoaded) {
            synchronized (mSyncObj) {
                boolean success = nDestroyEncoder();
                if (success) {
                    mIsLoaded = false;
                }
            }

        }
    }

    private native boolean nInitEncoder(int width, int height);
    private native int nEncode(long matNativeObj, byte[] outEncodedFrameBufferBytes);   // encoded version of frame is written to outEncodedFrameBufferBytes, returns the number of bytes in the encoded frame
    private native boolean nDestroyEncoder();
}
