package edu.purdue.andersed.streaming;

import org.opencv.core.Mat;

public interface DecodeListener {

    void onReceivedDecodedFrame(Mat decodedRGBFrame);
}
