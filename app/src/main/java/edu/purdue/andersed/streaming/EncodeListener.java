package edu.purdue.andersed.streaming;

public interface EncodeListener {

    void onFrameEncoded(byte[] encodedFrameBufferBytes, int numBytesInEncodedFrame);
}
