package edu.purdue.andersed.streaming;

/**
 * Created by andersed on 9/30/2016.
 */

/*
Represents a byte[] with an associated length.
Note that the actual byte[] size may be larger (but shouldn't be shorter) than the length variable.
This is because these arrays are buffers that may be larger than the actual messages contained in them.
 */
public class ByteArrayWithLength {

    byte[] mByteArray;
    int mLength;

    public ByteArrayWithLength(byte[] byteArray) {
        mByteArray = byteArray;
        mLength = byteArray.length; // initially set to the same value
    }

    public void setLength(int newLength) {
        assert (newLength <= mByteArray.length);
        mLength = newLength;
    }

    public byte[] getByteArray() {
        return mByteArray;
    }

    public int getLength() {
        return mLength;
    }
}
