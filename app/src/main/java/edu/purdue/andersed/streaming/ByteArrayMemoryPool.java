package edu.purdue.andersed.streaming;

import android.util.Log;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by andersed on 9/30/2016.
 */

/*
This class is used for keeping track of a fixed allocation of memory.

A set of N byte-arrays is created at runtime, each with a fixed size of S.

There is a "producer queue" and a "consumer queue." Initially, all N arrays are in the producer queue.

A producer can get a reference to the next buffer in the producer queue and copy data to it, automatically moving the buffer to the consumer queue.
If there are no buffers remaining in the producer queue at the moment, it does nothing and returns NULL.

A consumer can pull a buffer from the consumer queue.
Once the consumer is done with the buffer, it indicates that to the pool, and the buffer is returned to the producer queue.
If there are no buffers in the consumer queue at the moment, it does nothing and returns NULL.
 */
public class ByteArrayMemoryPool {

    private static final String TAG = "ByteArrayMemoryPool";

    private Set<ByteArrayWithLength> mBuffers;

    private Queue<ByteArrayWithLength> mProducerQueue;
    private Queue<ByteArrayWithLength> mConsumerQueue;

    public ByteArrayMemoryPool(int numBuffers, int numBytesPerBuffer) {
        Log.d(TAG, "creating ByteArrayMemoryPool for " + numBuffers + " buffers of size " + numBytesPerBuffer);
        mBuffers = new HashSet<ByteArrayWithLength>();

        for (int i = 0; i < numBuffers; i++) {
            mBuffers.add(new ByteArrayWithLength(new byte[numBytesPerBuffer]));
        }

        mProducerQueue = new ConcurrentLinkedQueue<ByteArrayWithLength>();
        mConsumerQueue = new ConcurrentLinkedQueue<ByteArrayWithLength>();

        for (ByteArrayWithLength buffer : mBuffers) {
            mProducerQueue.add(buffer);
        }
    }

    public ByteArrayWithLength producerPoll() {
        return mProducerQueue.poll();
    }

    public void handToConsumer(ByteArrayWithLength buffer) {
        mConsumerQueue.add(buffer);
    }

    public ByteArrayWithLength consumerPoll() {
        return mConsumerQueue.poll();
    }

    public void handToProducer(ByteArrayWithLength buffer) {
        mProducerQueue.add(buffer);
    }
}
