package edu.purdue.andersed.streaming;

import android.util.Log;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class Decoder {

    private Thread mDecoderLoopThread;
    private boolean mRunning;

    private DecodeListener mListener;

    private int mDecodeWidth;

    private int mDecodeHeight;

    private static final String TAG = "Decoder";

    private Mat mRGBMatDecoded;

    ByteArrayMemoryPool mPool;

    private static Decoder instance = new Decoder();

    private boolean mIsLoaded;

    static
    {
        try
        {
            Log.d(TAG, "loading streaming_video_manager");
            System.loadLibrary("hello-android-jni");
        }
        catch( UnsatisfiedLinkError e )
        {
            System.err.println("Native code library failed to load.\n" + e);
            throw new RuntimeException("unable to load JNI module.");
        }
    }

    private Decoder() {
        Log.d(TAG, "initializing Decoder");
    }

    public static Decoder getInstance() {
        if (instance == null) {
            instance = new Decoder();
        }
        return instance;
    }

    private void teardownIfNeeded() {
        mRunning = false;

        if (mDecoderLoopThread != null && mDecoderLoopThread.isAlive()) {
            Log.d(TAG, "interrupting thread");
            mDecoderLoopThread.interrupt();
        }
    }

    public void setPool(ByteArrayMemoryPool pool) {
        mPool = pool;
    }

    public void init(DecodeListener listener, int decodeWidth, int decodeHeight) {
        Log.d(TAG, "init");

        teardownIfNeeded();

        mDecoderLoopThread = new Thread(new DecoderLoop());

        mListener = listener;

        mDecodeWidth = decodeWidth;
        mDecodeHeight = decodeHeight;

        mIsLoaded = nInitDecoder(mDecodeWidth, mDecodeHeight);

        mRGBMatDecoded = new Mat(decodeHeight, decodeWidth, CvType.CV_8UC3);
        mPool = null;

        mRunning = true;

        mDecoderLoopThread.start();
    }

    public void destroy() {
        Log.d(TAG, "destroy");

        teardownIfNeeded();

        if (mIsLoaded) {
            boolean success = nDestroyDecoder();
            if (success) {
                mIsLoaded = false;
            }
        }

    }

    private class DecoderLoop implements Runnable {
        private static final String TAG = "DecoderLoop";

        public void run() {
            Log.d(TAG, "starting run()");
            while (mRunning && !Thread.currentThread().isInterrupted()) {
                if (mPool != null) {
                    ByteArrayWithLength packetToDecode = mPool.consumerPoll();
                    if (packetToDecode != null) {

                        //Log.d(TAG, "got a packet to decode of size " + packetToDecode.getLength() + ", decoding...");
                        boolean gotFrame = nDecode(packetToDecode.getByteArray(), packetToDecode.getLength(), mRGBMatDecoded.getNativeObjAddr());
                        //Log.d(TAG, "got a frame from the decode operation? " + gotFrame);
                        if (gotFrame) {

                            if (mListener != null) {
                                mListener.onReceivedDecodedFrame(mRGBMatDecoded);
                            }
                        }

                        mPool.handToProducer(packetToDecode);
                    }
                }
            }
            Log.d(TAG, "ending run()");
        }
    }

    private native boolean nInitDecoder(int width, int height);
    private native boolean nDecode(byte[] byteArrayToDecode, int numBytesToDecode, long matNativeObj);
    private native boolean nDestroyDecoder();
}
