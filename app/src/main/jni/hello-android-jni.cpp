// added to fix compilation error about UINT64_C not being defined (solution from https://github.com/AutonomyLab/ardrone_autonomy/issues/1 )
#ifndef UINT64_C
#define UINT64_C(c) (c ## ULL)
#endif

#ifdef __cplusplus
extern "C" {
#endif
// ffmpeg libraries are in C not C++, so this may help with proper linking
// https://stackoverflow.com/questions/15625468/libav-linking-error-undefined-references
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#ifdef __cplusplus
}
#endif

#include <GLES3/gl3.h>

#include <jni.h>
#include <android/log.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


using namespace cv;

#define  LOG_TAG    "hello-android-jni"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGE(...)   __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define CHECK_MAT(cond) if(!(cond)){ LOGI("FAILED: " #cond); return; }




extern "C" {






//=============================================================================

#define INBUF_SIZE 4096

// encoding:
AVCodecContext* encoder_c = NULL;
AVFrame *encoder_frame;
AVPacket encoder_pkt;
int encoder_i;
SwsContext* encoder_sws;

int ENCODER_WIDTH_PIXELS;
int ENCODER_HEIGHT_PIXELS;

//=============================================================================

JNIEXPORT jboolean JNICALL
Java_edu_purdue_andersed_streaming_Encoder_nInitEncoder(JNIEnv *env, jobject instance, jint width,
                                                        jint height) {

    LOGI("in nInit");

    ENCODER_WIDTH_PIXELS = width;
    ENCODER_HEIGHT_PIXELS = height;

    /* register all the codecs */
    avcodec_register_all();

    //enum AVCodecID codec_id = AV_CODEC_ID_MPEG4;
    enum AVCodecID codec_id = AV_CODEC_ID_MJPEG;
    LOGI("codec_id: %d", codec_id);

    AVCodec *codec;
    encoder_c = NULL;

    encoder_i = 0;


    /* find the video encoder */
    codec = avcodec_find_encoder(codec_id);
    if (!codec) {
        LOGE("codec not found");
        exit(1);
    }

    encoder_c = avcodec_alloc_context3(codec);
    if (!encoder_c) {
        LOGE("could not allocate video codec context");
        exit(1);
    }

    /* put sample parameters */
    encoder_c->bit_rate = 4000000;
    /* resolution must be a multiple of two */
    encoder_c->width = ENCODER_WIDTH_PIXELS;
    encoder_c->height = ENCODER_HEIGHT_PIXELS;
    /* frames per second */
    encoder_c->time_base = (AVRational){1,25};
    //encoder_c->gop_size = 10; /* emit one intra frame every ten frames */
    //encoder_c->max_b_frames = 1;
    //encoder_c->pix_fmt = AV_PIX_FMT_YUV420P;
    encoder_c->pix_fmt = AV_PIX_FMT_YUVJ420P;


    encoder_sws = sws_getContext(ENCODER_WIDTH_PIXELS, ENCODER_HEIGHT_PIXELS, AV_PIX_FMT_RGB24, ENCODER_WIDTH_PIXELS, ENCODER_HEIGHT_PIXELS, AV_PIX_FMT_YUVJ420P, SWS_FAST_BILINEAR, 0, 0, 0);

    if (codec_id == AV_CODEC_ID_H264) {
        av_opt_set(encoder_c->priv_data, "preset", "slow", 0);
    }

    /* open it */
    if (avcodec_open2(encoder_c, codec, NULL) < 0) {
        LOGE("Could not open codec");
        exit(1);
    }

    // here normally the output file would be opened, but we aren't going to write to a file

    encoder_frame = av_frame_alloc();
    if (!encoder_frame) {
        LOGE("Could not allocate video frame\n");
        exit(1);
    }
    encoder_frame->format = encoder_c->pix_fmt;
    encoder_frame->width  = encoder_c->width;
    encoder_frame->height = encoder_c->height;

    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    int ret = av_image_alloc(encoder_frame->data, encoder_frame->linesize, encoder_c->width, encoder_c->height,
                             encoder_c->pix_fmt, 32);
    if (ret < 0) {
        LOGE("Could not allocate raw picture buffer");
        exit(1);
    }

    // here is where we would encode each frame

    LOGI("done initing encoder");

    return JNI_TRUE;
}

JNIEXPORT jint JNICALL
Java_edu_purdue_andersed_streaming_Encoder_nEncode(JNIEnv *env, jobject instance,
                                                   jlong matNativeObj, jbyteArray outEncodedFrameBufferBytes) {
    //LOGI("start nEncode");

    cv::Mat* matToEncode = (cv::Mat*)matNativeObj;

    av_init_packet(&encoder_pkt);
    encoder_pkt.data = NULL;	// packet data will be allocated by the encoder
    encoder_pkt.size = 0;

    uint8_t* rgb24Data = matToEncode->data;
    uint8_t * inData[1] = { rgb24Data };	// rgb24 has one plane
    int inLinesize[1] = { 3*ENCODER_WIDTH_PIXELS };	// rgb stride

    sws_scale(encoder_sws, (const uint8_t* const*)inData, inLinesize, 0, ENCODER_HEIGHT_PIXELS, encoder_frame->data, encoder_frame->linesize);

    //encoder_frame->pts = encoder_i;	// TODO: check the timing to see what should be done here

    encoder_i++;

    int ret, got_output;

    // encode the image
    ret = avcodec_encode_video2(encoder_c, &encoder_pkt, encoder_frame, &got_output);
    if (ret < 0) {
        LOGE("error encoding frame");
        exit(1);
    }

    jint return_value;

    if (got_output) {
        const signed char* bufData = (const signed char*)encoder_pkt.data;
        if (bufData != NULL) {
            env->SetByteArrayRegion(outEncodedFrameBufferBytes, 0, encoder_pkt.size, bufData);
            return_value = encoder_pkt.size;
        } else {
            LOGE("bufData was null");
            return_value = -1;
        }
    } else {
        LOGE("didn't get output");
        return_value = -1;
    }

    av_free_packet(&encoder_pkt);

    //LOGI("end nEncode");

    return return_value;
}

JNIEXPORT jboolean JNICALL
Java_edu_purdue_andersed_streaming_Encoder_nDestroyEncoder(JNIEnv *env, jobject instance) {
    LOGI("destroyEncoder");

    LOGI("avcodec_close...");
    avcodec_close(encoder_c);

    LOGI("encoder_c: %p", encoder_c);
    if (encoder_c != NULL) {
        LOGI("av_free...");
        av_free(encoder_c);
    }
    LOGI("encoder_frame->data: %p", encoder_frame->data);
    if (encoder_frame->data != NULL) {
        LOGI("av_freep...");
        av_freep(&encoder_frame->data[0]);
    }
    LOGI("encoder_frame: %p", encoder_frame);
    if (encoder_frame != NULL) {
        LOGI("av_frame_free...");
        av_frame_free(&encoder_frame);
    }
    LOGI("encoder destroyed");

    return JNI_TRUE;
}

//=============================================================================

// decoding:
AVCodecContext *decoder_c = NULL;
AVFrame *decoder_frame;
AVPacket decoder_pkt;
SwsContext* decoder_sws;

int DECODER_WIDTH_PIXELS;
int DECODER_HEIGHT_PIXELS;

//=============================================================================

JNIEXPORT jboolean JNICALL Java_edu_purdue_andersed_streaming_Decoder_nInitDecoder(JNIEnv * env, jobject obj, int width, int height) {
    LOGI("in nInit");

    DECODER_WIDTH_PIXELS = width;
    DECODER_HEIGHT_PIXELS = height;

    /* register all the codecs */
    avcodec_register_all();

    //enum AVCodecID codec_id = AV_CODEC_ID_MPEG4;
    enum AVCodecID codec_id = AV_CODEC_ID_MJPEG;

    LOGI("in initDecoder()");

    AVCodec *decoder_codec;


    uint8_t inbuf[INBUF_SIZE + FF_INPUT_BUFFER_PADDING_SIZE];

    av_init_packet(&decoder_pkt);

    /* set end of buffer to 0 (this ensures that no overreading happens for damaged mpeg streams) */
    memset(inbuf + INBUF_SIZE, 0, FF_INPUT_BUFFER_PADDING_SIZE);

    /* find the mpeg1 video decoder */
    decoder_codec = avcodec_find_decoder(codec_id);
    if (!decoder_codec) {
        LOGE("Codec not found\n");
        exit(1);
    }

    decoder_c = avcodec_alloc_context3(decoder_codec);
    if (!decoder_c) {
        LOGE("Could not allocate video codec context\n");
        exit(1);
    }

    if(decoder_codec->capabilities&CODEC_CAP_TRUNCATED) {
        decoder_c->flags|= CODEC_FLAG_TRUNCATED; /* we do not send complete frames */
    }

    /* For some codecs, such as msmpeg4 and mpeg4, width and height
           MUST be initialized there because this information is not
           available in the bitstream. */

    /* open it */
    if (avcodec_open2(decoder_c, decoder_codec, NULL) < 0) {
        LOGE("Could not open codec\n");
        exit(1);
    }

    // here is where we would open the file from a filename, but we aren't reading from a file

    decoder_frame = av_frame_alloc();
    if (!decoder_frame) {
        LOGE("Could not allocate video frame\n");
        exit(1);
    }

    decoder_sws = sws_getContext(DECODER_WIDTH_PIXELS, DECODER_HEIGHT_PIXELS, AV_PIX_FMT_YUVJ420P, DECODER_WIDTH_PIXELS, DECODER_HEIGHT_PIXELS, AV_PIX_FMT_RGB24, SWS_FAST_BILINEAR, 0, 0, 0);

    // here is where we would decode each frame

    LOGI("done initing decoder");

    return JNI_TRUE;
}

JNIEXPORT jboolean JNICALL Java_edu_purdue_andersed_streaming_Decoder_nDecode(JNIEnv * env, jobject obj, jbyteArray byteArrayToDecode, jint numBytesToDecode, jlong matNativeObj) {
    //LOGI("Java_edu_purdue_andersed_streaming_Decoder_nDecode");
    jboolean returnValue = false;

    jbyte* bytesToDecode = env->GetByteArrayElements(byteArrayToDecode, NULL);
    jsize lengthOfArray = numBytesToDecode;

    if (lengthOfArray > 0) {

        decoder_pkt.size = lengthOfArray;
        decoder_pkt.data = (uint8_t*) bytesToDecode;

        int len;
        int got_frame;

        len = avcodec_decode_video2(decoder_c, decoder_frame, &got_frame, &decoder_pkt);

        if (len < 0) {
            LOGE("error while decoding frame");
            returnValue = false;
        }

        //LOGI("got_frame? %d", got_frame);

        if (got_frame) {
            //LOGI("starting sws_scale");
            cv::Mat* decodedMat = (cv::Mat*)matNativeObj;

            uint8_t* rgb24Data = decodedMat->data;
            uint8_t * outData[1] = { rgb24Data };	// rgb24 has one plane
            int outLinesize[1] = { 3*DECODER_WIDTH_PIXELS };	// rgb stride

            sws_scale(decoder_sws, (const uint8_t* const*)decoder_frame->data, decoder_frame->linesize, 0, DECODER_HEIGHT_PIXELS, outData, outLinesize);

            returnValue = true;
            //LOGI("ending sws_scale");
        }

    } else {
        LOGI("bytes to decode were empty, doing nothing");
    }

    //LOGI("releasing byte array");
    env->ReleaseByteArrayElements(byteArrayToDecode, bytesToDecode, 0);
    //LOGI("released byte array");

    return returnValue;
}

JNIEXPORT jboolean JNICALL Java_edu_purdue_andersed_streaming_Decoder_nDestroyDecoder(JNIEnv * env, jobject obj) {
    LOGI("destroyDecoder");

    LOGI("avcodec_close...");
    avcodec_close(decoder_c);
    LOGI("av_free...");
    av_free(decoder_c);
    LOGI("av_frame_free...");
    av_frame_free(&decoder_frame);
    LOGI("decoder destroyed");

    return JNI_TRUE;
}

#define DEBUG 1

#define BYTES_PER_PIXEL 4

void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
    GLenum err = glGetError();
    if (err != GL_NO_ERROR)
    {
        LOGI("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
        abort();
    }
}

#ifdef DEBUG
#define GL_CHECK(stmt) do { \
            stmt; \
            CheckOpenGLError(#stmt, __FILE__, __LINE__); \
        } while (0)
#else
#define GL_CHECK(stmt) stmt
#endif

JNIEXPORT jint JNICALL
Java_edu_purdue_andersed_star_PBOBuffer_doNativePBOSetup(JNIEnv *env, jobject instance,
                                                         jint pbo_size) {
    LOGI("starting doNativePBOSetup \n");

    LOGI("pbo_size: %i", pbo_size);

    GLuint pbo_id;



    GL_CHECK( glGenBuffers(1, &pbo_id) );

    GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo_id) );

    GL_CHECK( glBufferData(GL_PIXEL_PACK_BUFFER, pbo_size, 0, GL_DYNAMIC_READ) );

    GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, 0) );

    LOGI("pbo_id: %i", pbo_id);

    LOGI("ending doNativePBOSetup \n");

    return pbo_id;
}


JNIEXPORT void JNICALL
Java_edu_purdue_andersed_star_PointOverlay_doNativeGlReadPixels(JNIEnv *env, jobject instance,
                                                                jint current_frame_pbo_id,
                                                                jint previous_frame_pbo_id,
                                                                jint screenWidth, jint screenHeight,
                                                                jint pbo_size,
                                                                jlong workingImageMatPtr,
                                                                jint shrink_factor) {
    LOGI("starting doNativeGlReadPixels \n");

    LOGI("current_frame_pbo_id: %i", current_frame_pbo_id);

    LOGI("previous_frame_pbo_id: %i", previous_frame_pbo_id);

    Mat* workingImageMat = (Mat*) workingImageMatPtr;

    LOGI("binding pixel pack buffer to current frame's PBO \n");
    GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, current_frame_pbo_id) );

    LOGI("reading pixels \n");

    GL_CHECK( glReadPixels(0, 0, screenWidth, screenHeight, GL_RGBA, GL_UNSIGNED_BYTE, 0) );

    LOGI("unbinding pixel pack buffer \n");
    GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, 0) );

    LOGI("binding pixel pack buffer to previous frame's PBO \n");
    GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, previous_frame_pbo_id) );

    LOGI("mapping buffer range \n");

    GLubyte *ptr;
    GL_CHECK( ptr = (GLubyte *)glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, pbo_size, GL_MAP_READ_BIT) );

    LOGI("ptr from glMapBufferRange: %p", ptr);


    GLubyte *workingImageMatData = workingImageMat->data;


    LOGI("going to copy pixels, shrunk by a factor of %i", shrink_factor);
    int workingImageIndex = 0;
    int fullImageIndex = (screenHeight - 1) * BYTES_PER_PIXEL*screenWidth;
    for(int i = 0; i < workingImageMat->rows; i++) {
        for(int j = 0; j < workingImageMat->cols; j++) {
            //LOGI("i=%i, j=%i", i, j);
            //LOGI("workingImageIndex=%i, fullImageIndex=%i", workingImageIndex, fullImageIndex);
            //memcpy(workingImageMatData + workingImageIndex, ptr + fullImageIndex, BYTES_PER_PIXEL);
            if (fullImageIndex < pbo_size) {
                memcpy(workingImageMatData + workingImageIndex, ptr + fullImageIndex, BYTES_PER_PIXEL);
            } else {
                LOGI("fullImageIndex is too big: %i", fullImageIndex);
            }

            workingImageIndex += BYTES_PER_PIXEL;
            fullImageIndex += (BYTES_PER_PIXEL*shrink_factor);
        }
        fullImageIndex -= ((shrink_factor+1)*BYTES_PER_PIXEL*screenWidth);
    }


    /*
    memcpy(workingImageMatData, ptr, workingImageMat->rows * workingImageMat->cols * BYTES_PER_PIXEL);
    */

    LOGI("unmapping buffer \n");
    GL_CHECK( glUnmapBuffer(GL_PIXEL_PACK_BUFFER) );

    LOGI("unbinding pixel pack buffer \n");
    GL_CHECK( glBindBuffer(GL_PIXEL_PACK_BUFFER, 0) );

    LOGI("ending doNativeGlReadPixels \n");
}

JNIEXPORT jstring JNICALL
Java_edu_purdue_andersed_star_JNITest_getMsgFromJni(JNIEnv *env, jobject instance) {

    // TODO


    return env->NewStringUTF("Hello From Jni");
}


};